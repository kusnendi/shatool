/**
 * Created by nendi on 27/09/14.
 */

function task_done(id){
    $.get("/done/"+id, function(data){
        if(data == "OK"){
            var current_status = $('#label_'+id).text();
            var status = 'Complete';
            var new_status = current_status.replace(current_status,status);

            /*** Update Class Text Title ***/
            $("#span_"+id).removeClass('text-red');
            $("#span_"+id).addClass('text-success');
            /*** Update Status & Class Status ***/
            $("#label_"+id).text(new_status);
            $("#label_"+id).removeClass('label-danger');
            $("#label_"+id).addClass('label-success');
            /*** Update Button Is_Complete ***/
            $("#is_complete"+id).removeClass('fa-check-square-o')
            $("#is_complete"+id).addClass('fa-check-square')

        }
    });
}

function delete_task(id){
    $.get("/delete/"+id, function(data){
        if(data == "OK"){
            var target = $('#'+id);
            target.hide('slow', function(){ target.remove(); });
        }
    });
}

function show_form(form_id){
    $("form").hide();
    $('#'+form_id).show("slow");
}

function edit_task(id,title){
    $('#edit_task_id').val(id);
    $('#edit_task_title').val(title);
    show_form('edit_task');
}

$('#add_task').submit(function(event){
    /* stop form from submitting normal */
    event.preventDefault();

    var title = $('#task_title').val();
    if(title){
        //ajax post the form
        $.post("/add", {title: title}).done(function(data){
            $('#add_task').hide("slow");
            $('#task_list').append(data);
        })
    }
    else {
        alert("please give a title to task");
    }
});

$('#edit_task').submit(function(event){
    /* stop form from submitting normal */
    event.preventDefault();

    var task_id = $('#edit_task_id').val();
    var title   = $('#edit_task_title').val();
    var current_title = $('#span_'+task_id).text();
    var new_title = current_title.replace(current_title,title);
    if(title){
        //ajax post the form
        $.post("/update/"+task_id, {title: title}).done(function(data){
            $('#edit_task').hide('slow');
            $('#span_'+task_id).text(new_title);
        });
    } else {
        alert("Please give a title to task.");
    }
});

$('#ApplyJob').submit(function(event){
    //stop from submiting normal
    event.preventDefault();
    var from = $('#fromMonth'), end = $('#endMonth'), year = $('#intYear'), btn = $('#Btn'), wrapper = $('#WrapperApply'), container = $('#ApplyReport');
    $('#LoaderApply').fadeIn('slow');
    $('#WrapMapping').fadeOut('slow');
    //pra process submiting form
    //from.setAttr('readonly','readonly');
    //end.setAttr('readonly','readonly');
    //year.setAttr('readonly','readonly')
    btn.removeClass('btn-info').addClass('btn-danger').text('Please Wait...');
    wrapper.hide('slow');
    $.post('/job/apply-jobs',{from: from.val(), end: end.val(), intYear: year.val()}).done(function(data){
        if (data){
            $('#LoaderApply').fadeOut('slow');
            btn.removeClass('btn-danger').addClass('btn-info').text('Filter');
            container.html(data).fadeIn('slow',setTimeout(function(){
                wrapper.show('slow');
            }),700);
            //alert(data);
        }
    });
});

function mappingApply(tanggal){
    //user var data
    var tgl = tanggal, wrapper = $('#WrapMapping'), container = $('#MainMapping'), title = $('#titleMapping');
    $('#LoaderMaps').fadeIn('slow');
    wrapper.fadeOut('slow');
    $.post('/job/mapping-apply',{tgl: tgl}).done(function(data){
        if (data){
            $('#LoaderMaps').fadeOut('slow');
            title.text('Mapping User Apply Job Periode '+tanggal);
            container.html(data).fadeIn('slow',setTimeout(function(){
                wrapper.show('slow').fadeIn('slow');
            }), 700);
        }
    });
}

function ApplyMailSent(tanggal){
    //user var data
    var tgl = tanggal, wrapper = $('#EmailDetail'), container = $('#DetailApplyJob'), title = $('#titleDetail');
    //$('#LoaderMaps').fadeIn('slow');
    wrapper.fadeOut('slow');
    $.post('/applyjob/log-email-detail',{tanggal: tgl}).done(function(data){
        if (data){
            //$('#LoaderMaps').fadeOut('slow');
            title.text('Detail Sent Email Apply Job Tanggal '+tanggal);
            container.html(data).fadeIn('slow',setTimeout(function(){
                wrapper.show('slow').fadeIn('slow');
            }), 700);
        }
    });
}