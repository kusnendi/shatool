<?php
/**
 * Created by PhpStorm.
 * User: nendi
 * Date: 30/09/14
 * Time: 11:03
 */

class LogApplyJob extends Eloquent{
    /* table used by this model */
    protected $table = 'email_log_applied_jobs';

    public function Job(){
        return $this->belongsTo('Job','code_jobs','code');
    }

    public function User(){
        return $this->belongsTo('UserProfile','user_id','user_id');
    }

    public function Company(){
        return $this->belongsTo('Company','id_company');
    }
}