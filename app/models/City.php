<?php
/**
 * Created by PhpStorm.
 * User: nendi
 * Date: 30/09/14
 * Time: 11:03
 */

class City extends Eloquent{
    /* table used by this model */
    protected $table = 'cities';

    public function Province(){
        return $this->belongsTo('Province','province_id');
    }
} 