<?php
/**
 * Created by PhpStorm.
 * User: nendi
 * Date: 29/09/14
 * Time: 21:53
 */

class LogCall extends Eloquent{
    /* table used by this model */
    protected $table = 'log_calls';

    //disable timestamps
    public $timestamps = false;

    public function userProfil(){
        return $this->belongsTo('UserProfile','user_id','user_id');
    }

    public function CallMe(){
        return $this->belongsTo('LokerCm','cm_id','id');
    }

} 