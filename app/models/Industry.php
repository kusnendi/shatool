<?php
/**
 * Created by PhpStorm.
 * User: nendi
 * Date: 30/09/14
 * Time: 11:03
 */

class Industry extends Eloquent{
    /* table used by this model */
    protected $table = 'industries';

    public function Company(){
        return $this->belongsTo('Company','industry_id');
    }
} 