<?php
/**
 * Created by PhpStorm.
 * User: nendi
 * Date: 30/09/14
 * Time: 11:03
 */

class Specialist extends Eloquent{
    /* table used by this model */
    protected $table = 'specialists';

    public function Job(){
        return $this->belongsTo('Job','job_id');
    }

    public function User(){
        return $this->belongsTo('UserProfile','user_id');
    }
}