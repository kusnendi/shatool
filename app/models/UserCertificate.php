<?php
/**
 * Created by PhpStorm.
 * User: Adicipta
 * Date: 10/27/2014
 * Time: 2:12 PM
 */

class UserCertificate extends Eloquent{

    protected $table = 'user_certificates';

    //disable timestamps
    public $timestamps = false;

    public function User(){
        return $this->belongsTo('User','user_id');
    }
} 