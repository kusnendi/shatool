<?php
/**
 * Created by PhpStorm.
 * User: nendi
 * Date: 30/09/14
 * Time: 11:03
 */

class ApplyJob extends Eloquent{
    /* table used by this model */
    protected $table = 'apply_jobs';

    public function Job(){
        return $this->belongsTo('Job','job_id');
    }

    public function User(){
        return $this->belongsTo('UserProfile','user_id','user_id');
    }

    public function Specialist(){
        return $this->belongsTo('Specialist','specialist_id');
    }

    public function Company(){
        return $this->belongsTo('Company','company_id');
    }
} 