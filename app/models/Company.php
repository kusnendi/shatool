<?php
/**
 * Created by PhpStorm.
 * User: nendi
 * Date: 29/09/14
 * Time: 21:53
 */

class Company extends Eloquent{
    /* table used by this model */
    protected $table = 'companies';

    public function Jobs(){
        return $this->hasMany('Job','company_id');
    }
} 