<?php
/**
 * Created by PhpStorm.
 * User: Adicipta
 * Date: 10/27/2014
 * Time: 2:12 PM
 */

class UserProfile extends Eloquent{

    protected $table = 'user_profiles';

    //disable timestamps
    public $timestamps = false;

    protected $guarded = array('id', 'user_id');

    public function User(){
        return $this->belongsTo('User','user_id');
    }
} 