<?php
/**
 * Created by PhpStorm.
 * User: nendi
 * Date: 30/09/14
 * Time: 11:03
 */

class Source extends Eloquent{
    /* table used by this model */
    protected $table = 'sources';

    /* disable auto_timestamp */
    public $timestamps = false;
} 