<?php
/**
 * Created by PhpStorm.
 * User: nendi
 * Date: 29/09/14
 * Time: 21:37
 */

class Job extends Eloquent{
    /* Table used this model */
    protected $table = 'jobs';

    public function User(){
        return $this->belongsTo('User','user_id');
    }

    public function Company(){
        return $this->belongsTo('Company','company_id');
    }

    public function City(){
        return $this->belongsTo('City','city_id');
    }

    public function ApplyJob(){
        return $this->hasMany('ApplyJob','id','job_id');
    }

    public function Specialist(){
        return $this->belongsTo('Specialist','specialist_id');
    }
} 