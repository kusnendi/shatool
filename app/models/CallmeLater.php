<?php
/**
 * Created by PhpStorm.
 * User: Adicipta
 * Date: 10/27/2014
 * Time: 2:12 PM
 */

class CallmeLater extends Eloquent{

    protected $table = 'callme_laters';

    public function CallMe(){
        return $this->belongsTo('LokerCm','cm_id');
    }

    public function User(){
        return $this->belongsTo('User','user_id');
    }
} 