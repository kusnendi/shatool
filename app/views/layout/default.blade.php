<!doctype html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>{{ $title }}</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport' />
        {{ HTML::style('assets/css/bootstrap.min.css') }}
        {{ HTML::style('assets/css/AdminLTE.css') }}
        {{ HTML::style('assets/css/font-awesome.css') }}
        {{ HTML::style('assets/css/ionicons.min.css') }}
        {{ HTML::style('assets/css/bootstrap-datetimepicker.min.css') }}
        {{--javascript--}}
        {{ HTML::script('assets/js/jquery.min.js') }}
        {{ HTML::script('assets/js/fusioncharts-jquery-plugin.js') }}
        {{ HTML::script('assets/js/Chart.js') }}
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
        <style>
            .green {
                color: #5cb85c;
            }

            .red {
                color: #d9534f;
            }
        </style>
    </head>
    <body>
        <div class="content">
        @yield('main')
        </div>

        {{ HTML::script('assets/js/bootstrap.min.js') }}
        {{ HTML::script('assets/js/bootstrap-datetimepicker.min.js') }}
        {{ HTML::script('assets/js/locales/bootstrap-datetimepicker.pt-BR.js') }}
        <script type="text/javascript">
          $(function() {
            $('.form_datetime').datetimepicker({
              format: 'yyyy-mm-dd hh:ii:ss'
            });
          });
        </script>
        <script type="text/javascript">
                  $(function() {
                    $('.form_date').datetimepicker({
                      format: 'yyyy-mm-dd'
                    });
                  });
                </script>
        {{ HTML::script('assets/js/todo.js') }}
    </body>
</html>