@extends('layout.default')

@section('main')
    <div class="row"> {{--Navigations--}}
        <div class="col-md-12">
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div>
              <ul class="nav nav-tabs nav-justified">
                <li class="active"><a href="#">Call Me</a></li>
                <li><a href="/appointment">Appointment</a></li>
              </ul>
            </div>
        </div>
    </div>
    @if (count($arrAppt))
    <div class="row">
        <div class="col-md-12">
            {{--Notification--}}
            <div class="alert alert-danger" style="margin-left: 0px; margin-top: 15px;">
                <p>Halo CS, silakan menghubungi pelanggan Tloker dgn msisdn <strong>{{ $arrAppt->hand_phone }}</strong> pada <strong>{{ date('d/m/Y H:i',strtotime($arrAppt->callme_back)) }} WIB</strong> untuk pembuatan CV di Tloker, Klik <a href="/appointment/edit/{{$arrAppt->id}}" class="alert-link">disini</a> untuk process appointment. Terima kasih. Zaenal</p>
            </div> {{--/Notification--}}
        </div>
    </div>
    @endif
    @if (Session::has('msg'))
    <div class="row">
        <div class="col-md-12">
            {{--Notification--}}
            <div class="alert alert-info" style="margin-left: 0px; margin-top: 15px;">
                <p>{{ Session::get('msg') }}</p>
            </div> {{--/Notification--}}
        </div>
    </div>
    @endif
    <h2 class="page-header">Monitoring Call Me</h2>
    <table class="table">
        <tr>
            <th class="col-md-1">REQUESTED : <a href="/callme">{{$count['requested']}}</a></th>
            <th class="col-md-1">CM001 : <a href="/callme/lists/2">{{$count['callme1']}}</a></th>
            <th class="col-md-1">CM002 : <a href="/callme/lists/3">{{$count['callme2']}}</a></th>
            <th class="col-md-1">CM003 : <a href="/callme/lists/4">{{$count['callme3']}}</a></th>
            <th class="col-md-1">QUARANTINE : <a href="/callme/lists/5">{{$count['karantina']}}</a></th>
            <th class="col-md-1">COMPLETED : <a href="/callme/lists/1">{{$count['completed']}}</a></th>
        </tr>
    </table>
    {{--Search Box--}}
    <div class="row">
        <div class="col-md-12">
            <div class="callout callout-info">
                <form method="get">
                    <div class="row form-group">
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="msisdn" placeholder="Ketik No Msisdn disini..." pattern="[0-9]{11}|[0-9]{12}|[0-9]{13}"/>
                        </div>
                        <div class="col-md-1">
                            <button type="submit" class="btn btn-info">Cari</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--/Search Box--}}
    @if($callmes->count())
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="col-md-1">No</th>
                        <th class="col-md-1">Tanggal</th>
                        <th class="col-md-1">Msisdn</th>
                        <th class="col-md-1">Reg</th>
                        <th class="col-md-1">Flag</th>
                        <th class="col-md-1">File</th>
                        <th class="col-md-3">Notes</th>
                        <th class="col-md-1">CM001</th>
                        <th class="col-md-1">CM002</th>
                        <th class="col-md-1">CM003</th>
                        <th class="col-md-1">COMPLETED</th>
                        <th class="col-md-1">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($callmes as $cm)
                    <tr>
                        <td>{{sprintf("%04s",$cm->id)}}</td>
                        <td>{{$cm->created_at}}</td>
                        <td>{{$cm->hand_phone}}</td>
                        @if (isset($cm->User))
                            <td>Ya</td>
                        @else
                            <td>Tidak</td>
                        @endif
                        <td>{{$status[$cm->flag]}}</td>
                        <td>
                        @if ($cm['isUpload'])
                            Uploaded
                        @else
                            -
                        @endif
                        </td>
                        <td>{{$cm->notes}}</td>
                        <td>
                            @if (count($cm['callme1']))
                                <strong>{{$cm['callme1'][0]['user_profil']['firstname']}}</strong><br>
                                {{$cm['callme1'][0]['created']}}<br>
                                @if ($cm->flag == 2 && $cm->is_blast == 1)
                                    {{--<a href="#" class="btn btn-sm btn-success disabled"><i class="fa fa-check-square-o"></i> Reminder</a>--}}
                                    <a href="\callme\reminder\{{$cm->id}}" class="btn btn-success btn-sm"><i class="fa fa-check-square-o"></i> Reminder</a>
                                @else
                                    <a href="#" class="btn btn-sm btn-success disabled"><i class="fa fa-check-square-o"></i> Reminder</a>
                                @endif
                            @endif
                        </td>
                        <td>
                            @if (count($cm['callme2']))
                                <strong>{{$cm['callme2'][0]['user_profil']['firstname']}}</strong><br>
                                {{$cm['callme2'][0]['created']}}<br>
                                @if ($cm->flag == 3 && $cm->is_blast == 1)
                                    {{--<a href="#" class="btn btn-sm btn-success disabled"><i class="fa fa-check-square-o"></i> Reminder</a>--}}
                                    <a href="\callme\reminder\{{$cm->id}}" class="btn btn-success btn-sm"><i class="fa fa-check-square-o"></i> Reminder</a>
                                @else
                                    <a href="#" class="btn btn-sm btn-success disabled"><i class="fa fa-check-square-o"></i> Reminder</a>
                                @endif
                            @endif
                        </td>
                        <td>
                            @if (count($cm['callme3']))
                                <strong>{{$cm['callme3'][0]['user_profil']['firstname']}}</strong><br>
                                {{$cm['callme3'][0]['created']}}<br>
                                @if ($cm->flag == 4 && $cm->is_blast == 1)
                                    {{--<a href="#" class="btn btn-sm btn-success disabled"><i class="fa fa-check-square-o"></i> Reminder</a>--}}
                                    <a href="\callme\reminder\{{$cm->id}}" class="btn btn-success btn-sm"><i class="fa fa-check-square-o"></i> Reminder</a>
                                @else
                                    <a href="#" class="btn btn-sm btn-success disabled"><i class="fa fa-check-square-o"></i> Reminder</a>
                                @endif
                            @endif
                        </td>
                        <td>
                            @if (count($cm['complete']))
                                <strong>{{$cm['complete'][0]['user_profil']['firstname']}}</strong><br>
                                {{$cm['complete'][0]['created']}}
                            @endif
                        </td>
                        <td>
                        {{--Button Action Follow Up--}}
                        @if ($cm->flag == 0)
                            <a href="/callme/process/{{$cm->id.'/'.$cm->hand_phone}}"><i class="fa fa-check"></i></a>
                        @elseif ($cm->flag == 1)
                            <a href="#"><i class="fa fa-thumbs-o-up"></i></a>
                        @else
                            <a href="/callme/process/{{$cm->id.'/'.$cm->hand_phone}}"><i class="fa fa-check-square-o"></i></a>
                        @endif
                        {{--Add Button Quarantine @ CM3
                        @if ($cm->flag == 4)
                            <a href="/callme/quarantine/{{$cm->id}}"><i class="fa fa-trash-o"></i></a>
                        @endif
                        --}}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{$callmes->appends(array_except(Input::query(), Paginator::getPageName()))->links()}}
        </div>
    </div>
    @endif
@stop