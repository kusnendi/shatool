@extends('layout.default')

@section('main')
    {{--Search Box--}}
    <div class="row">
        <div class="col-md-12">
            <div class="callout callout-danger">
                <form method="get">
                    <div class="row form-group">
                        <div class="col-md-1">
                            <input type="text" class="form-control" name="month" value="{{$intMonth}}" placeholder="Bulan" pattern="[0-9]{2}"/>
                        </div>
                        <div class="col-md-1">
                            <input type="text" class="form-control" name="year" value="{{$intYear}}" placeholder="Tahun" pattern="[0-9]{4}"/>
                        </div>
                        <div class="col-md-1">
                            <button type="submit" class="btn btn-danger">Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--/Search Box--}}

    <div class="row">
        <div class="col-md-6">
            <h4 style="border-bottom: 1px solid #dedede; padding-bottom: 3px;"><i class="fa fa-dashboard"></i>&nbsp;&nbsp;&nbsp;Call Me Summary</h4>
            <div class="row">
                <div class="col-md-4"> {{--New--}}
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3>{{$intNew['current']}}/{{$intNew['requested']}}/{{$intNew['all']}}</h3>
                            <p>Requested</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-document"></i>
                        </div>
                        <a class="small-box-footer" href="/callme">Click here <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>

                <div class="col-md-4"> {{--Quarantine--}}
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3>{{$intQrt['current']}}</h3>
                            <p>Quarantine</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-trash-a"></i>
                        </div>
                        <a class="small-box-footer" href="/callme/lists/5">Click here <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>

                <div class="col-md-4"> {{--Complete--}}
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3>{{$intCom['current']}}</h3>
                            <p>Complete</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-checkmark-circled"></i>
                        </div>
                        <a class="small-box-footer" href="/callme/lists/1">Click here <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>

                <div class="col-md-4"> {{--CM1--}}
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3>{{$intCM1['current']}}/{{$intCM1['followup']}}</h3>
                            <p>CM01</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-ios7-telephone"></i>
                        </div>
                        <a class="small-box-footer" href="/callme/lists/2">Click here <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>

                <div class="col-md-4"> {{--CM2--}}
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3>{{$intCM2['current']}}/{{$intCM2['followup']}}</h3>
                            <p>CM02</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-ios7-telephone"></i>
                        </div>
                        <a class="small-box-footer" href="/callme/lists/3">Click here <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>

                <div class="col-md-4"> {{--CM3--}}
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3>{{$intCM3['current']}}/{{$intCM3['followup']}}</h3>
                            <p>CM03</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-ios7-telephone"></i>
                        </div>
                        <a class="small-box-footer" href="/callme/lists/4">Click here <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-solid">
                        <div class="box-header">
                            <i class="fa fa-user"></i>
                            <h3 class="box-title">CS Stats</h3>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-4"></div>
                            </div>
                            <table class="table" style="font-size:12px; margin-top: 5px; border-top: 0px">
                                <tr style="border-bottom: 3px solid #ccc; border-top: 0px">
                                    <th>No</th>
                                    <th class="col-md-4">Customer Service</th>
                                    <th class="col-md-2">CM01</th>
                                    <th class="col-md-2">CM02</th>
                                    <th class="col-md-2">CM03</th>
                                    <th class="col-md-2">Completed</th>
                                    <th class="col-md-2">Total</th>
                                </tr>
                                @foreach($arrCs as $key => $cs)
                                    <?php $key++ ?>
                                    <tr>
                                        <td>{{$key}}</td>
                                        <td>{{$cs->UserProfil->realname}}</td>
                                        <td><a href="/callme/csdailyreport/{{$cs->id}}?month={{$intMonth}}&year={{$intYear}}"><small class="label label-success" style="font-size:11px;">{{sprintf("%03s",$cs['cm1'])}}</small></a></td>
                                        <td><a href="/callme/csdailyreport/{{$cs->id}}?month={{$intMonth}}&year={{$intYear}}"><small class="label label-success" style="font-size:11px;">{{sprintf("%03s",$cs['cm2'])}}</small></a></td>
                                        <td><a href="/callme/csdailyreport/{{$cs->id}}?month={{$intMonth}}&year={{$intYear}}"><small class="label label-success" style="font-size:11px;">{{sprintf("%03s",$cs['cm3'])}}</small></a></td>
                                        <td><a href="/callme/csdailyreport/{{$cs->id}}?month={{$intMonth}}&year={{$intYear}}"><small class="label label-success" style="font-size:11px;">{{sprintf("%03s",$cs['com'])}}</small></a></td>
                                        <td>
                                        <?php
                                            $total = $cs['cm1']+$cs['cm2']+$cs['cm3']+$cs['com'];
                                        ?>
                                        <small class="label label-success" style="font-size:11px;">{{sprintf("%03s",$total)}}</small>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-solid">
                        <div class="box-header">
                            <i class="fa fa-users"></i>
                            <h3 class="box-title">Analisis CV Complete</h3>
                        </div>
                        <div class="box-body">
                        @if (count($arrData))
                            <table class="table">
                                <tr>
                                @foreach ($arrData as $data)
                                    @if (is_null($data->stat))
                                        <td><span style="font-size:12px">REQ - COM:</span> <small class="label label-info" style="font-size:14px;">{{$percent = number_format(($data->jumlah/$intNew['requested'])*100,1)}} %</small></td>
                                    @elseif ($data->stat == 'CALLME01')
                                        <td><span style="font-size:12px">CM1 - COM:</span> <small class="label label-info" style="font-size:14px;">{{$percent = number_format(($data->jumlah/$intNew['requested'])*100,1)}} %</small></td>
                                    @elseif ($data->stat == 'CALLME02')
                                        <td><span style="font-size:12px">CM2 - COM:</span> <small class="label label-info" style="font-size:14px;">{{$percent = number_format(($data->jumlah/$intNew['requested'])*100,1)}} %</small></td>
                                    @elseif ($data->stat == 'CALLME03')
                                        <td><span style="font-size:12px">CM3 - COM:</span> <small class="label label-info" style="font-size:14px;">{{$percent = number_format(($data->jumlah/$intNew['requested'])*100,1)}} %</small></td>
                                    @endif
                                @endforeach
                                </tr>
                            </table>
                        @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="box box-solid">
                        <div class="box-header">
                            <i class="fa fa-users"></i>
                            <h3 class="box-title">Daily Request</h3>
                        </div>
                        <div class="box-body">
                            <table class="table" style="font-size:12px; margin-top: 5px; border-top: 0px">
                                <tr style="border-bottom: 3px solid #ccc; border-top: 0px">
                                    <th class="col-md-2">Tanggal</th>
                                    <th class="col-md-1">Requested</th>
                                    <th class="col-md-1">CM01</th>
                                    <th class="col-md-1">CM02</th>
                                    <th class="col-md-1">CM03</th>
                                    <th class="col-md-1">Completed</th>
                                </tr>
                                @foreach($arrCM as $cm)
                                <tr>
                                    <td>{{$cm->tanggal}}</td>
                                    <td><a href="/callme/summary?date={{$cm->tanggal}}"><small class="label label-info" style="font-size:11px;">{{sprintf("%03s",$cm->jumlah)}}</small></a></td>
                                    <td>
                                    @if ($cm['cm1'])
                                        <a href="/callme/daily/callme01?date={{$cm->tanggal}}"><small class="label label-warning" style="font-size:11px;">{{sprintf("%03s",$cm['cm1'])}}</small></a>
                                    @else
                                        <small class="label label-warning" style="font-size:11px;">{{sprintf("%03s",$cm['cm1'])}}</small>
                                    @endif
                                    </td>
                                    <td>
                                    @if ($cm['cm2'])
                                        <a href="/callme/daily/callme02?date={{$cm->tanggal}}"><small class="label label-warning" style="font-size:11px;">{{sprintf("%03s",$cm['cm2'])}}</small></a>
                                    @else
                                        <small class="label label-warning" style="font-size:11px;">{{sprintf("%03s",$cm['cm2'])}}</small>
                                    @endif
                                    </td>
                                    <td>
                                    @if ($cm['cm3'])
                                        <a href="/callme/daily/callme03?date={{$cm->tanggal}}"><small class="label label-warning" style="font-size:11px;">{{sprintf("%03s",$cm['cm3'])}}</small></a>
                                    @else
                                        <small class="label label-warning" style="font-size:11px;">{{sprintf("%03s",$cm['cm3'])}}</small>
                                    @endif
                                    </td>
                                    <td>
                                    @if ($cm['com'])
                                        <a href="/callme/daily/out016?date={{$cm->tanggal}}"><small class="label label-success" style="font-size:11px;">{{sprintf("%03s",$cm['com'])}}</small></a>
                                    @else
                                        <small class="label label-success" style="font-size:11px;">{{sprintf("%03s",$cm['com'])}}</small>
                                    @endif
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                            {{$arrCM->appends(array_except(Input::query(), Paginator::getPageName()))->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop