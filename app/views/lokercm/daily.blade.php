@extends('layout.default')

@section('main')
    {{--Search Box--}}
    <div class="row">
        <div class="col-md-8">
            <div class="callout callout-danger">
                <form method="get">
                    <div class="row form-group">
                        <div class="col-md-5">
                            <input type="text" class="form-control" name="date" value="{{$intDate}}" placeholder="dd-mm-yyyy"/>
                        </div>
                        <div class="col-md-1">
                            <button type="submit" class="btn btn-danger">Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--/Search Box--}}

    <div class="row">
        <div class="col-md-8">
            <div class="box box-solid">
                <div class="box-header">
                    <i class="fa fa-info-circle"></i>
                    <h3 class="box-title">Log Detail Call Me ({{$arrCode}}), Tanggal {{$intDate}} </h3>
                </div>
                <div class="box-body">
                    <table class="table" style="font-size:12px; margin-top: 5px; border-top: 0px">
                        <tr style="border-bottom: 3px solid #ccc; border-top: 0px">
                            <th class="col-md-1">MSISDN</th>
                            <th class="col-md-1">Tgl. Request</th>
                            <th class="col-md-1">Customer Service</th>
                            <th class="col-md-4">Catatan</th>
                        </tr>
                        @foreach ($arrData as $data)
                        <tr>
                            <td>{{$data->msisdn}}</td>
                            <td>{{$data->CallMe->created_at}}</td>
                            <td>{{$data->userProfil->realname}}</td>
                            <td>{{$data->notes}}</td>
                        </tr>
                        @endforeach
                    </table>
                    {{$arrData->appends(array_except(Input::query(), Paginator::getPageName()))->links()}}
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="row">

            </div>
        </div>
    </div>
    <a href="/callme/report" class="btn btn-danger btn-sm"><i class="fa fa-mail-reply">&nbsp;Kembali</i></a>
@stop