@extends('layout.default')

@section('main')
    {{--Search Box--}}
    <div class="row">
        <div class="col-md-8">
            <div class="callout callout-danger">
                <form method="get">
                    <div class="row form-group">
                        <div class="col-md-5">
                            <input type="text" class="form-control" name="date" value="{{$intDate}}" placeholder="dd-mm-yyyy"/>
                        </div>
                        <div class="col-md-1">
                            <button type="submit" class="btn btn-danger">Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--/Search Box--}}

    <div class="row">
        <div class="col-md-8">
            <div class="box box-solid">
                <div class="box-header">
                    <i class="fa fa-bar-chart-o"></i>
                    <h3 class="box-title">Request Analisis</h3>
                </div>
                <div class="box-body">
                    <table class="table" style="font-size:12px; margin-top: 5px; border-top: 0px">
                        <tr style="border-bottom: 3px solid #ccc; border-top: 0px">
                            <th class="col-md-2">MSISDN</th>
                            <th class="col-md-1">Requested</th>
                            <th class="col-md-1">CM01</th>
                            <th class="col-md-1">CM02</th>
                            <th class="col-md-1">CM03</th>
                            <th class="col-md-1">Completed</th>
                        </tr>
                        @foreach ($arrData as $data)
                        <tr>
                            <td>{{$data->hand_phone}}</td>
                            <td><i class="fa fa-check-circle" style="font-size:16px; color: #5cb85c"></i></td>
                            <td>
                                @if ($data['cm1'])
                                    <i class="fa fa-check-circle" style="font-size:16px; color: #5cb85c"></i>
                                @else
                                    <i class="fa fa-times-circle" style="font-size:16px; color: #f56954"></i>
                                @endif
                            </td>
                            <td>
                                @if ($data['cm2'])
                                    <i class="fa fa-check-circle" style="font-size:16px; color: #5cb85c"></i>
                                @else
                                    <i class="fa fa-times-circle" style="font-size:16px; color: #f56954"></i>
                                @endif
                            </td>
                            <td>
                                @if ($data['cm3'])
                                    <i class="fa fa-check-circle" style="font-size:16px; color: #5cb85c"></i>
                                @else
                                    <i class="fa fa-times-circle" style="font-size:16px; color: #f56954"></i>
                                @endif
                            </td>
                            <td>
                                @if ($data['com'])
                                    <i class="fa fa-check-circle" style="font-size:16px; color: #5cb85c"></i>
                                @else
                                    <i class="fa fa-times-circle" style="font-size:16px; color: #f56954"></i>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </table>
                    {{$arrData->appends(array_except(Input::query(), Paginator::getPageName()))->links()}}
                </div>
            </div>
        </div>
    </div>
    <a href="/callme/report" class="btn btn-danger btn-sm"><i class="fa fa-mail-reply">&nbsp;Kembali</i></a>
@stop