@extends('layout.default')

@section('main')
    <h2 class="page-header">Monitoring Call Me</h2>
    <table class="table">
        <tr>
            <th class="col-md-1">REQUESTED : <a href="/callme">{{$count['requested']}}</a></th>
            <th class="col-md-1">CM001 : <a href="/callme/lists/2">{{$count['callme1']}}</a></th>
            <th class="col-md-1">CM002 : <a href="/callme/lists/3">{{$count['callme2']}}</a></th>
            <th class="col-md-1">CM003 : <a href="/callme/lists/4">{{$count['callme3']}}</a></th>
            <th class="col-md-1">COMPLETED : <a href="/callme/lists/1">{{$count['completed']}}</a></th>
        </tr>
    </table>
    @if($callmes->count())
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="col-md-1">No</th>
                        <th class="col-md-1">Tanggal</th>
                        <th class="col-md-1">Msisdn</th>
                        <th class="col-md-1">Reg</th>
                        <th class="col-md-1">Flag</th>
                        <th class="col-md-1">File</th>
                        <th class="col-md-3">Notes</th>
                        <th class="col-md-1">CM001</th>
                        <th class="col-md-1">CM002</th>
                        <th class="col-md-1">CM003</th>
                        <th class="col-md-1">COMPLETED</th>
                        <th class="col-md-1">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($callmes as $cm)
                    <tr>
                        <td>{{sprintf("%04s",$cm->id)}}</td>
                        <td>{{$cm->created_at}}</td>
                        <td>{{$cm->hand_phone}}</td>
                        @if (isset($cm->User))
                            <td>Ya</td>
                        @else
                            <td>Tidak</td>
                        @endif
                        <td>{{$status[$cm->flag]}}</td>
                        <td>
                            {{----}}
                        </td>
                        <td>{{$cm->notes}}</td>
                        <td>
                            {{----}}
                        </td>
                        <td>
                            {{----}}
                        </td>
                        <td>
                            {{----}}
                        </td>
                        <td>
                            {{----}}
                        </td>
                        <td>
                        @if ($cm->flag == 0)
                            <a href="/callme/process/{{$cm->id.'/'.$cm->hand_phone}}"><i class="fa fa-check"></i></a>
                        @elseif ($cm->flag == 1)
                            <a href="#"><i class="fa fa-thumbs-o-up"></i></a>
                        @else
                            <a href="/callme/process/{{$cm->id.'/'.$cm->hand_phone}}"><i class="fa fa-check-square-o"></i></a>
                        @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{$callmes->appends(array_except(Input::query(), Paginator::getPageName()))->links()}}
        </div>
    </div>
    @endif
@stop