@extends('layout.default')

@section('main')
    {{--Search Box--}}
    <div class="row">
        <div class="col-md-12">
            <div class="callout callout-danger">
                <form method="get" action="/callme/report">
                    <div class="row form-group">
                        <div class="col-md-1">
                            <input type="text" class="form-control" name="month" value="{{--$intMonth--}}" placeholder="Bulan" pattern="[0-9]{2}"/>
                        </div>
                        <div class="col-md-2">
                            <input type="text" class="form-control" name="year" value="{{--$intYear--}}" placeholder="Tahun" pattern="[0-9]{4}"/>
                        </div>
                        <div class="col-md-1">
                            <button type="submit" class="btn btn-danger">Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--/Search Box--}}

    {{--CM Summary--}}
    <div class="row">
        <div class="col-md-2"> {{--New--}}
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>{{$intNew['current']}}/{{$intNew['requested']}}</h3>
                    <p>Requested</p>
                </div>
                <div class="icon">
                    <i class="ion ion-document"></i>
                </div>
                <a class="small-box-footer" href="#">Click here <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-md-2"> {{--CM1--}}
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>{{$intCM1['followup']}}/{{$intCM1['current']}}</h3>
                    <p>CM01</p>
                </div>
                <div class="icon">
                    <i class="ion ion-ios7-telephone"></i>
                </div>
                <a class="small-box-footer" href="#">Click here <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-md-2"> {{--CM2--}}
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>{{$intCM2['followup']}}/{{$intCM2['current']}}</h3>
                    <p>CM02</p>
                </div>
                <div class="icon">
                    <i class="ion ion-ios7-telephone"></i>
                </div>
                <a class="small-box-footer" href="#">Click here <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-md-2"> {{--CM3--}}
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>{{$intCM3['followup']}}/{{$intCM3['current']}}</h3>
                    <p>CM03</p>
                </div>
                <div class="icon">
                    <i class="ion ion-ios7-telephone"></i>
                </div>
                <a class="small-box-footer" href="#">Click here <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-md-2"> {{--Quarantine--}}
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>{{$intQrt['current']}}</h3>
                    <p>Quarantine</p>
                </div>
                <div class="icon">
                    <i class="ion ion-trash-a"></i>
                </div>
                <a class="small-box-footer" href="#">Click here <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-md-2"> {{--Complete--}}
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>{{$intCom['current']}}</h3>
                    <p>Complete</p>
                </div>
                <div class="icon">
                    <i class="ion ion-checkmark-circled"></i>
                </div>
                <a class="small-box-footer" href="#">Click here <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-header">
                    <i class="ion ion-person"></i>
                    <h3 class="box-title">CS Stats</h3>
                </div>
                <div class="box-body">
                    <table class="table" style="font-size:12px; margin-top: 5px; border-top: 0px">
                        <tr style="border-bottom: 3px solid #ccc; border-top: 0px">
                            <th>No</th>
                            <th class="col-md-4">Customer Service</th>
                            <th class="col-md-2">CM01</th>
                            <th class="col-md-2">CM02</th>
                            <th class="col-md-2">CM03</th>
                            <th class="col-md-2">Completed</th>
                        </tr>
                        @foreach($arrCs as $key => $cs)
                            <?php $key++ ?>
                            <tr>
                                <td>{{$key}}</td>
                                <td>{{$cs->UserProfil->realname}}</td>
                                <td>{{$cs['cm1']}}</td>
                                <td>{{$cs['cm2']}}</td>
                                <td>{{$cs['cm3']}}</td>
                                <td>{{$cs['com']}}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop