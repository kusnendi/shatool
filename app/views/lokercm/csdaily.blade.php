@extends('layout.default')

@section('main')
    {{--Search Box--}}
    <div class="row">
        <div class="col-md-8">
            <div class="callout callout-danger">
                <form method="get">
                    <div class="row form-group">
                        <div class="col-md-2">
                            <input type="text" class="form-control" name="month" value="{{$intMonth}}" placeholder="Bulan" pattern="[0-9]{2}"/>
                        </div>
                        <div class="col-md-2">
                            <input type="text" class="form-control" name="year" value="{{$intYear}}" placeholder="Tahun" pattern="[0-9]{4}"/>
                        </div>
                        <div class="col-md-1">
                            <button type="submit" class="btn btn-danger">Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--/Search Box--}}

    <div class="row">
        <div class="col-md-8">
            <div class="box box-solid">
                <div class="box-header">
                    <i class="fa fa-user"></i>
                    <h3 class="box-title">CS Daily Summary</h3>
                </div>
                <div class="box-body">
                    <table class="table" style="font-size:12px; margin-top: 5px; border-top: 0px">
                        <tr style="border-bottom: 3px solid #ccc; border-top: 0px">
                            <th class="col-md-1">Tanggal</th>
                            <th class="col-md-1">CM01</th>
                            <th class="col-md-1">CM02</th>
                            <th class="col-md-1">CM03</th>
                            <th class="col-md-1">Completed</th>
                        </tr>
                        @foreach ($arrCs as $cs)
                        <tr>
                            <td>{{$cs->tanggal}}</td>
                            <td><a href="/callme/daily/callme01?cs={{$cs->user_id}}&date={{$cs->tanggal}}"><small class="label label-info" style="font-size:12px;">{{sprintf("%02s",$cs['cm1'])}}</small></a></td>
                            <td><a href="/callme/daily/callme02?cs={{$cs->user_id}}&date={{$cs->tanggal}}"><small class="label label-info" style="font-size:12px;">{{sprintf("%02s",$cs['cm2'])}}</small></a></td>
                            <td><a href="/callme/daily/callme03?cs={{$cs->user_id}}&date={{$cs->tanggal}}"><small class="label label-info" style="font-size:12px;">{{sprintf("%02s",$cs['cm3'])}}</small></a></td>
                            <td><a href="/callme/daily/out016?cs={{$cs->user_id}}&date={{$cs->tanggal}}"><small class="label label-success" style="font-size:12px;">{{sprintf("%02s",$cs['com'])}}</small></a></td>
                        </tr>
                        @endforeach
                    </table>
                    {{$arrCs->appends(array_except(Input::query(), Paginator::getPageName()))->links()}}
                </div>
            </div>
        </div>
    </div>
    <a href="/callme/report" class="btn btn-danger btn-sm"><i class="fa fa-mail-reply">&nbsp;Kembali</i></a>
@stop