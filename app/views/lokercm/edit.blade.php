@extends('layout.default')

@section('main')
    <div class="row">
        <div class="col-md-6">
            <div class="box box-danger">
                <div class="box-header">
                    <h3 class="box-title">Update Call Me</h3>
                </div>
                <div class="box-body">
                    @if(Session::get('password'))
                        <div class="alert alert-success" style="margin-left:0">
                            <p>Silahkan gunakan user & password berikut untuk melengkapi CV<br>
                            User : <strong>{{Session::get('msisdn')}}</strong>, Password : <strong>{{Session::get('password')}}</strong></p>
                        </div>
                    @else
                        <div class="alert alert-success" style="margin-left:0">
                            <p>Silahkan gunakan msisdn ini <strong>{{Session::get('msisdn')}}</strong> di MSISDN Log untuk mengetahui password user<br>
                        </div>
                    @endif
                    <form class="form" method="post" action="/callme/update/{{$callme->id}}">
                        <div class="form-group">
                            <label>MSISDN</label>
                            <input class="form-control input-sm" name="hand_phone" type="text" value="{{$callme->hand_phone}}" readonly>
                        </div>
                        <div class="form-group">
                            <label>Catatan</label>
                            <textarea name="notes" class="form-control input-sm" placeholder="Ketik catatan disini..."></textarea>
                        </div>
                        <div class="form-group">
                            <label>Status</label>
                            {{ Form::select('flag', $status, $callme->flag, array('class' => 'form-control input-sm')) }}
                        </div>
                        <div class="form-group">
                            <input name="add_appointment" type="checkbox" value="1"> <strong>Add Appointment</strong>
                        </div>
                        <div class="form-group">
                            <p>Notes:<br><small>Klik <strong>Add Appointment</strong> jika user meminta untuk di hubungi kembali pada jam tertentu.</small></p>
                        </div>
                        <?php
                        /*
                        <div class="form-group">
                            <label>Secret</label>
                            <input class="form-control input-sm" name="secret" type="text" value="">
                        </div>
                        */
                        ?>
                        <div class="form-group">
                            <button class="btn btn-danger btn-flat btn-sm" type="submit"><i class="fa fa-save"></i>&nbsp;Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop