@extends('layout.default')

@section('main')
    <div class="row"> {{--Navigations--}}
        <div class="col-md-12">
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div>
              <ul class="nav nav-tabs nav-justified">
                <li><a href="/callme">Call Me</a></li>
                <li class="active"><a href="#">Appointment</a></li>
              </ul>
            </div>
        </div>
    </div>
    @if (count($arrAppt))
    <div class="row">
        <div class="col-md-12">
            {{--Notification--}}
            <div class="alert alert-danger" style="margin-left: 0px; margin-top: 15px;">
                <p>Halo CS, silakan menghubungi pelanggan Tloker dgn msisdn <strong>{{ $arrAppt->hand_phone }}</strong> pada <strong>{{ date('d/m/Y H:i',strtotime($arrAppt->callme_back)) }} WIB</strong> untuk pembuatan CV di Tloker, Klik <a href="/appointment/edit/{{$arrAppt->id}}" class="alert-link">disini</a> untuk process appointment. Terima kasih. Zaenal</p>
            </div> {{--/Notification--}}
        </div>
    </div>
    @endif
    <h2 class="page-header">Monitoring Appointment</h2>
    @if($cmLaters->count())
    <div class="row">
        <div class="col-md-10">
            <div class="callout callout-info">
                <form method="get">
                    <div class="row form-group">
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="msisdn" placeholder="Msisdn" pattern="[0-9]{11}|[0-9]{12}|[0-9]{13}"/>
                        </div>
                        {{--
                        <div class="col-md-4">
                            <label>From</label>
                            <div class="input-group date form_datetime">
                                <input type="text" class="form-control" data-format="yyyy-mm-dd hh:ii:ss" name="start_date" required/>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>To</label>
                            <div class="input-group date form_datetime">
                                <input type="text" class="form-control" data-format="yyyy-mm-dd hh:ii:ss" name="end_date" required/>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        --}}
                        <div class="col-md-1">
                            <button type="submit" class="btn btn-info">Cari</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="col-md-1">No</th>
                        <th class="col-md-2">Msisdn</th>
                        <th class="col-md-3">Tanggal</th>
                        <th class="col-md-6">Notes</th>
                        <th class="col-md-2">Created</th>
                        <th class="col-md-2">Modified</th>
                        <th class="col-md-1">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($cmLaters as $cm)
                    <tr class="{{ $cm['class'] }}">
                        <td>{{sprintf("%04s",$cm->id)}}</td>
                        <td>{{$cm->hand_phone}}</td>
                        <td>{{$cm->callme_back}}</td>
                        <td>{{$cm->notes}}</td>
                        <td>{{$cm->User->email}}</td>
                        <td>{{$cm['updated']}}</td>
                        <td>
                        @if ($cm->status == 0)
                            <a href="/appointment/edit/{{$cm->id}}"><i class="fa fa-check"></i></a>
                        @else
                            <a href="#"><i class="fa fa-thumbs-o-up"></i></a>
                        @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{$cmLaters->appends(array_except(Input::query(), Paginator::getPageName()))->links()}}
        </div>
    </div>
    @else
    <div class="callout callout-info">
        <p>Silahkan kembali lagi nanti, untuk saat ini belum ada data appointment. </p><p>Terimakasih<br/><strong>Support</strong></p>
    </div>
    @endif
@stop