@extends('layout.default')

@section('main')
    <div class="row">
        <div class="col-md-6">
            <div class="box box-danger">
                <div class="box-header">
                    <h3 class="box-title">Follow Up</h3>
                </div>
                <div class="box-body">
                    <form class="form" method="post" action="/appointment/update">
                        <input name="id" type="hidden" value="{{$cm->id}}">
                        <input name="cm_id" type="hidden" value="{{$cm->cm_id}}">
                        <div class="form-group">
                            <label>MSISDN</label>
                            <input class="form-control input-sm" name="hand_phone" type="text" value="{{$cm->hand_phone}}" readonly>
                        </div>
                        <div class="form-group">
                            <label>Tanggal</label>
                            <div class="input-group date form_datetime">
                                <input type="text" class="form-control" data-format="yyyy-mm-dd hh:ii:ss" name="callme_back" value="{{$cm->callme_back}}" readonly/>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Catatan</label>
                            <textarea name="notes" class="form-control input-sm" placeholder="Ketik catatan disini..."></textarea>
                        </div>
                        <div class="form-group">
                            <label>Status</label>
                            {{ Form::select('flag', $status, $callme->flag, array('class' => 'form-control input-sm')) }}
                        </div>
                        <div class="form-group">
                            <button class="btn btn-danger btn-flat btn-sm" type="submit"><i class="fa fa-save"></i>&nbsp;Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop