<table class="table table-bordered table-striped" style="margin-bottom: 0px">
    <tr>
        <th>No</th>
        <th class="col-md-3">Applicant</th>
        <th class="col-md-5">Judul Lowongan</th>
        <th class="col-md-4">Perusahaan</th>
    </tr>
    <?php $i=1; ?>
    @foreach($LogEmailDetail as $email)
        <tr>
            <td>{{$i}}</td>
            <td>{{$email->User->realname}}</td>
            <td>{{$email->Job->title}}</td>
            <td>{{$email->company_id}}</td>
        </tr>
        <?php $i++ ?>
    @endforeach
</table>