@extends('layout.default')

@section('main')
    <h2 class="page-header">Detail Applicant Apply Jobs</h2>
    {{--@if($applyLaters->count())--}}
    <div class="row">
        <div class="col-md-12">
            <div class="callout callout-danger">
                <form method="get">
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label>From</label>
                            <div class="input-group date form_date">
                                <input type="text" class="form-control" data-format="yyyy-mm-dd" name="start_date" value="{{$intDate1}}" required/>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-1" style="padding-top: 25px">
                            <button type="submit" class="btn btn-danger">Cari</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Applicant</th>
                        <th>Judul Lowongan</th>
                        <th>Perusahaan</th>
                        <th>Fungsi Kerja</th>
                        <th>Media</th>
                        <th class="text-center">Status</th>
                    </tr>
                </thead>
                <tbody>
                <?php $i=1; ?>
                @foreach($Applies as $apply)
                    <tr>
                        <td>{{$i}}</td>
                        <td>{{$apply->User->realname}}</td>
                        <td>{{$apply->Job->title}}</td>
                        <td>{{$apply->Company->name}}</td>
                        <td>{{$apply->Specialist->name}}</td>
                        <td>@if ($apply->flag == 0) SMS @else WEB @endif</td>
                        <td class="text-center">@if (count($apply['Status'])) <small class="label label-success">Email Sent</small> @else <small class="label label-danger">Email Not Sent</small> @endif</td>
                    </tr>
                    <?php $i++ ?>
                @endforeach
                </tbody>
            </table>
            {{$Applies->appends(array_except(Input::query(), Paginator::getPageName()))->links()}}
            <a href="/applyjob" class="btn btn-danger"><i class="fa fa-reply"></i> Kembali</a>
        </div>
    </div>
    {{--@else
    <div class="callout callout-info">
        <p>Silahkan kembali lagi nanti, untuk saat ini belum ada data appointment. </p><p>Terimakasih<br/><strong>Support</strong></p>
    </div>
    @endif
    --}}
@stop