@extends('layout.default')

@section('main')
    <h2 class="page-header">Monitoring Sent Email Apply Jobs</h2>
    {{--@if($applyLaters->count())--}}
    <div class="row">
        <div class="col-md-12">
            <div class="callout callout-danger">
                <form method="get">
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label>From</label>
                            <div class="input-group date form_date">
                                <input type="text" class="form-control" data-format="yyyy-mm-dd" name="start_date" value="{{$intDate1}}" required/>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>To</label>
                            <div class="input-group date form_date">
                                <input type="text" class="form-control" data-format="yyyy-mm-dd" name="end_date" value="{{$intDate2}}" required/>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-1" style="padding-top: 25px">
                            <button type="submit" class="btn btn-danger">Cari</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            @if (count($Applies))
            <h4>Summary Email Sent Apply Jobs</h4>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="col-md-3">Tanggal</th>
                        <th class="col-md-2 text-center">Jumlah Apply</th>
                        <th class="col-md-2 text-center">Email Sent</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($Applies as $apply)
                    <tr>
                        <td>{{$apply->tanggal}}</td>
                        <td class="text-center"><a href="/applyjob/applicant-detail?start_date={{$apply->tanggal}}">{{$apply->jumlah}}</a></td>
                        <td class="text-center"><a href="javascript:void(0);" onClick="ApplyMailSent('{{$apply->tanggal}}');">{{$apply['intEmailSent']}}</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{--$applyLaters->appends(array_except(Input::query(), Paginator::getPageName()))->links()--}}
            @endif
        </div>
        <div id="EmailDetail" class="col-md-8" style="display:none">
            <h4 id="titleDetail">Detail Email Sent Apply Jobs</h4>
            <div id="DetailApplyJob"></div>
        </div>
    </div>
    {{--@else
    <div class="callout callout-info">
        <p>Silahkan kembali lagi nanti, untuk saat ini belum ada data appointment. </p><p>Terimakasih<br/><strong>Support</strong></p>
    </div>
    @endif
    --}}
@stop