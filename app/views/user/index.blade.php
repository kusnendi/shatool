@extends('layout.default')

@section('main')
    <div class="row">
        <div class="col-md-12">
            <h2 class="page-header">User | CV Not Complete</h2>
            {{--Search Box--}}
            <div class="row">
                <div class="col-md-12">
                    <div class="callout callout-info">
                        <form method="get">
                            <h4>Quick Search</h4>
                            <div class="row form-group">
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="msisdn" placeholder="Ketik No Msisdn disini..." pattern="[0-9]{11}|[0-9]{12}|[0-9]{13}"/>
                                </div>
                                <div class="col-md-1">
                                    <button type="submit" class="btn btn-info">Cari</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            {{--/Search Box--}}
            <div class="row"> {{--Navigations--}}
                <div class="col-md-12">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div>
                      <ul class="nav nav-tabs nav-justified">
                        <li class="active"><a href="#">Not Yet Follow Up</a></li>
                        <li><a href="/user/list">Followed</a></li>
                      </ul>
                    </div>
                </div>
            </div>
            @if(User::count())
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="col-md-4">Handphone</th>
                            <th class="col-md-4">Email</th>
                            <th class="col-md-4">Subscription</th>
                            <th class="col-md-1">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{$user->hand_phone}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->sms_subscription}}</td>
                                <td>
                                    <form method="post" target="_parent" action="http://tloker.com/admin/activities/logall">
                                        <input type="hidden" name="data[Activity][search]" value="{{$user->hand_phone}}">
                                        <button type="submit" class="btn btn-primary btn-flat btn-sm">Process</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{$users->links()}}
            @else
                <h4>There are no user</h4>
            @endif
        </div>
    </div>
@stop()