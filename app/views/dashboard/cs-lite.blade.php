@extends('layout.default')

@section('main')
<div class="row"> {{--newsticker--}}
    <div class="col-md-12">
        <div class="box box-solid box-info">
            <div class="box-header">
                <div class="row">
                    <div class="col-md-2">
                        <h3 class="box-title">Perhatian</h3>
                    </div>
                    <div class="col-md-10">
                        <marquee direction="left" behavior="scroll" scrollamount="3" onMouseOver='this.stop();' OnMouseOut='this.start();'>
                            <h3 class="box-title">Hi {{$arrUsr->realname}}, Last login : {{date('d - m - Y H:i:s')}}. Dashboard Beta Version, dear team cs jika menemukan error mohon untuk di screenshot dan di kirim ke it@tloker.com. Terimakasih :D</h3>
                        </marquee>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-9">
        {{--create tabs welcome--}}
        <div class="box box-solid">
            <div class="box-header">
                <i class="ion ion-person"></i>
                <h3 class="box-title">Activities Overview</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-4"> {{--requested--}}
                        <div class="small-box bg-blue">
                            <div class="inner">
                                <h3>{{ $arrOvr['inc'] }}</h3>
                                <p>Incoming Call</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-ios7-telephone"></i>
                            </div>
                            <a class="small-box-footer" href="http://tloker.com/admin/logcalls" target="_parent">Click here <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>

                    <div class="col-md-4"> {{--callme01--}}
                        <div class="small-box bg-purple">
                            <div class="inner">
                                <h3>{{ $arrOvr['out'] }}</h3>
                                <p>Outgoing Call</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-ios7-telephone"></i>
                            </div>
                            <a class="small-box-footer" href="http://tloker.com/admin/logcalls" target="_parent">Click here <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>

                    <div class="col-md-4"> {{--callme02--}}
                        <div class="small-box bg-green">
                            <div class="inner">
                                <h3>{{ $arrOvr['job'] }}</h3>
                                <p>Job Posted</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-briefcase"></i>
                            </div>
                            <a class="small-box-footer" href="http://tloker.com/admin/jobs/add" target="_parent">Click here <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div> {{--/tabs welcome--}}
    </div>

    <div class="col-md-3">
        {{--create tabs todo list--}}
        <div class="box box-primary">
            <div class="box-header">
                <i class="ion ion-clipboard"></i>
                <h3 class="box-title">Todo List</h3>
            </div>
            <div class="box-body">
                <ul class="todo-list ui-sortable">
                    <li><span class="handle"><i class="fa fa-ellipsis-v"></i> <i class="fa fa-ellipsis-v"></i></span><span class="text">Post Job Vacancies</span> <small class="label label-danger">40/day</small></li>
                    <li><span class="handle"><i class="fa fa-ellipsis-v"></i> <i class="fa fa-ellipsis-v"></i></span><span class="text">Follow up cv not complete</span></li>
                    <li><span class="handle"><i class="fa fa-ellipsis-v"></i> <i class="fa fa-ellipsis-v"></i></span><span class="text">Follow up call me</span> <small class="label label-danger">urgent</small></li>
                </ul>
            </div>
        </div> {{--/tabs welcome--}}
    </div>
</div>

{{--Stats Overview--}}
<div class="row">
    <div class="col-md-9">
        {{--box callme--}}
        <div class="box box-solid box-danger">
            <div class="box-header">
                <i class="ion ion-ios7-telephone"></i>
                <h3 class="box-title">Call Me | Stats Overview</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3"> {{--requested--}}
                        <div class="small-box bg-red">
                            <div class="inner">
                                <h3>{{ $arrCM['req'] }}</h3>
                                <p>REQUESTED</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-person-stalker"></i>
                            </div>
                            <a class="small-box-footer" href="http://tloker.com/admin/cs/callme/0" target="_parent">Click here <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>

                    <div class="col-md-3"> {{--callme01--}}
                        <div class="small-box bg-aqua">
                            <div class="inner">
                                <h3>{{ $arrCM['cm1'] }}</h3>
                                <p>CALLME01</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-person"></i>
                            </div>
                            <a class="small-box-footer" href="http://tloker.com/admin/cs/callme/2" target="_parent">Click here <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>

                    <div class="col-md-3"> {{--callme02--}}
                        <div class="small-box bg-yellow">
                            <div class="inner">
                                <h3>{{ $arrCM['cm2'] }}</h3>
                                <p>CALLME02</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-person"></i>
                            </div>
                            <a class="small-box-footer" href="http://tloker.com/admin/cs/callme/3" target="_parent">Click here <i class="fa fa-arrow-circle-right"></i></a>
                            {{--<a class="small-box-footer" href="http://localhost:8080/admin/cs/callme/3" target="_parent">Click here <i class="fa fa-arrow-circle-right"></i></a>--}}
                        </div>
                    </div>

                    <div class="col-md-3"> {{--callme02--}}
                        <div class="small-box bg-maroon">
                            <div class="inner">
                                <h3>{{ $arrCM['cm3'] }}</h3>
                                <p>CALLME03</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-person"></i>
                            </div>
                            <a class="small-box-footer" href="http://tloker.com/admin/cs/callme/4" target="_parent">Click here <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div> {{--/box callme--}}
    </div>
</div>
@stop