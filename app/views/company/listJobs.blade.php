@extends('layout.default')

@section('main')
    <h2 class="page-header">Jobs Management | List Job</h2>

    @if ($jobs->count())
    <div class="row">
        <div class="col-md-12">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Code</th>
                    <th>Company</th>
                    <th>Job Title</th>
                    <th>Posted</th>
                    <th>Expired</th>
                </tr>
            </thead>
            <tbody>
                @foreach($jobs as $job)
                <tr>
                    <td>{{HTML::link('job/detail/'.$job->code,$job->code)}}</td>
                    <td>{{$job->Company->name}}</td>
                    <td>{{$job->title}}</td>
                    <td>{{$job->created}}</td>
                    <td>{{$job->expired}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{$jobs->links()}}
        </div>
    </div>
    @else
    <div class="alert alert-danger">
        <p>There are no job founded, please come back later!</p>
    </div>
    @endif
@stop