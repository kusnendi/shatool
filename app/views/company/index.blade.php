@extends('layout.default')

@section('main')
    <h2 class="page-header">Company</h2>
    @if($companies->count())
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Website</th>
                        <th>Email</th>
                        <th>Jobs</th>
                        <th>Created</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($companies as $company)
                        <tr>
                            <td>{{$company->id}}</td>
                            <td>{{$company->name}}</td>
                            <td>{{$company->address}}</td>
                            <td>{{$company->website}}</td>
                            <td>{{$company->email}}</td>
                            <td><a href="{{url('company/listjobs/'.$company->id)}}">{{$company->Jobs->count()}}</a></td>
                            <td>{{date('d/m/Y',strtotime($company->created))}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{$companies->links()}}
            </div>
        </div>
    @endif
@stop