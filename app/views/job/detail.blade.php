@extends('layout.default')

@section('main')
    <h2 class="page-header">Detail Lowongan Kerja</h2>
    @if ($job->count())
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">{{$job->Company->name}}</h3>
                </div>
                <div class="box-body">
                    {{-- Position Title --}}
                    <h4 class="panel-title">Judul Lowongan</h4>
                    <p>{{$job->title}}</p>
                    {{-- Description --}}
                    <h4 class="panel-title">Description</h4>
                    <p>{{$job->description}}</p>
                    {{-- Kota --}}
                    <h4 class="panel-title">Lokasi</h4>
                    <p>{{$job->City->name}}</p>
                    {{-- Persyaratan Utama --}}
                    <h4 class="panel-title">Persyaratan Utama</h4>
                    <p>{{$job->primary_job_role}}</p>
                    {{-- Expired --}}
                    <h4 class="panel-title">Gaji</h4>
                    @if ($job->min_salary == 0 || $jo->max_salary == 0)
                        <p>Negotiable</p>
                    @else
                        <p>{{$job->min_salary}} - {{$job->max_salary}}</p>
                    @endif
                    {{-- Expired --}}
                    <h4 class="panel-title">Expired</h4>
                    <p>{{date('d/m/Y',strtotime($job->expired))}}</p>
                </div>
            </div> {{-- ./Box --}}
        </div>
    </div>
    @endif
@stop