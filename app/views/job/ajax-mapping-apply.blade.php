<table class="table table-bordered table-striped" style="margin-bottom: 0px">
    <tr>
        <th>From Date CV</th>
        <th>Jumlah <small>(Unique User)</small></th>
        <th class="text-center">CV</th>
        <th class="text-center">%</th>
        <th class="text-center">Is Reg</th>
        <th class="text-center">Is Email</th>
    </tr>
    @foreach($Maps as $map)
        <tr>
            <td>{{$map->tanggal}}</td>
            <td class="text-center">{{$map->jumlah}}</td>
            <td class="text-center">{{count($map['cv'])}}</td>
            <td class="text-center">{{number_format(($map->jumlah/count($map['cv']))*100,1,',','.')}} %</td>
            <td class="text-center">{{$map['isReg']}}</td>
            <td class="text-center">{{$map['isEmail']}}</td>
        </tr>
    @endforeach
</table>