<div class="col-md-6">
    <div class="row">
        <div class="col-xs-12">
            <div id="LoaderApply" class="text-center" style="display: none">
                <img src="/assets/images/ajax-loader.gif" />
            </div>
            <div id="WrapperApply" class="panel panel-primary" style="min-height: 293px">
                <div class="panel-heading">
                    <h4 class="panel-title">Overview Apply Jobs</h4>
                </div>
                <div id="MainContent" class="panel-body">
                    <table class="table table-bordered table-striped" style="margin-bottom: 0px">
                        <tr>
                            <th>Bulan</th>
                            <th>SMS</th>
                            <th>WEB</th>
                            <th>CV</th>
                            <th>Akumulasi CV</th>
                            <th>Total Apply</th>
                        </tr>
                        @foreach($arrApplys as $apply)
                            <tr>
                                <td>{{$apply->tanggal}}</td>
                                <td>{{$apply['sms']}}</td>
                                <td>{{$apply['web']}}</td>
                                <td>{{count($apply['cv'])}}</td>
                                <td>{{count($apply['totalcv'])}}</td>
                                <td><a href="javascript:void(0);" onClick="mappingApply('{{$apply->tanggal}}');">{{$apply->jumlah}}</a></td>
                            </tr>
                        @endforeach
                        <tr>
                            <td>Total</td>

                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div id="LoaderMaps" class="text-center" style="display: none">
                <img src="/assets/images/ajax-loader.gif" />
            </div>
            <div id="WrapMapping" class="panel panel-danger" style="display: none; min-height: 293px">
                <div class="panel-heading">
                    <h4 id="titleMapping" class="panel-title"></h4>
                </div>
                <div id="MainMapping" class="panel-body">

                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div id="WrapperCities" class="panel panel-primary" style="min-height: 293px">
                <div class="panel-heading">
                    <h4 class="panel-title">Top 5 Pendidikan</h4>
                </div>
                <div id="MainContent" class="panel-body">
                    <table class="table table-bordered table-striped" style="margin-bottom: 0px">
                        <tr>
                            <th class="col-xs-10">Pendidikan</th>
                            <th class="col-xs-2 text-center">Jumlah Apply</th>
                        </tr>
                        @foreach($Educations as $e)
                            @if ($e->last_education == '')
                                <tr>
                                    <td>N/A</td>
                                    <td class="text-center">{{$e->jumlah}}</td>
                                </tr>
                            @elseif($e->last_education == null)
                                <tr>
                                    <td>Invalid</td>
                                    <td class="text-center">{{$e->jumlah}}</td>
                                </tr>
                            @else
                                <tr>
                                    <td>{{$e->last_education}}</td>
                                    <td class="text-center">{{$e->jumlah}}</td>
                                </tr>
                            @endif
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-6">
    <div class="row">
        <div class="col-xs-12">
            <div id="WrapperSpecialist" class="panel panel-primary" style="min-height: 293px">
                <div class="panel-heading">
                    <h4 class="panel-title">Top 10 Specialist</h4>
                </div>
                <div id="MainContent" class="panel-body">
                    <table class="table table-bordered table-striped" style="margin-bottom: 0px">
                        <tr>
                            <th class="col-xs-10">Specialist</th>
                            <th class="col-xs-2 text-center">Jumlah Apply</th>
                        </tr>
                        @foreach($Specialists as $specialist)
                            <tr>
                                <td>{{$specialist->Specialist->name}}</td>
                                <td class="text-center">{{$specialist->jumlah}}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div id="WrapperCities" class="panel panel-primary" style="min-height: 293px">
                <div class="panel-heading">
                    <h4 class="panel-title">Top 10 Kota</h4>
                </div>
                <div id="MainContent" class="panel-body">
                    <table class="table table-bordered table-striped" style="margin-bottom: 0px">
                        <tr>
                            <th class="col-xs-10">Kota</th>
                            <th class="col-xs-2 text-center">Jumlah Apply</th>
                        </tr>
                        @foreach($Cities as $city)
                            <tr>
                                <td>{{$city->name}}</td>
                                <td class="text-center">{{$city->jumlah}}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div id="LoaderMaps" class="text-center" style="display: none">
                <img src="/assets/images/ajax-loader.gif" />
            </div>
            <div id="WrapMapping" class="panel panel-danger" style="display: none; min-height: 293px">
                <div class="panel-heading">
                    <h4 id="titleMapping" class="panel-title"></h4>
                </div>
                <div id="MainMapping" class="panel-body">

                </div>
            </div>
        </div>
    </div>
</div>