@extends('layout.default')

@section('main')
    {{--Search Box--}}
    <div class="row">
        <div class="col-md-12">
            <div class="callout callout-info">
                <form method="get">
                    <div class="row form-group">
                        <div class="col-md-1">
                            <input type="text" class="form-control" name="month" value="{{$intMonth}}" placeholder="Bulan" pattern="[0-9]{2}"/>
                        </div>
                        <div class="col-md-2">
                            <input type="text" class="form-control" name="year" value="{{$intYear}}" placeholder="Tahun" pattern="[0-9]{4}"/>
                        </div>
                        <div class="col-md-1">
                            <button type="submit" class="btn btn-info">Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--/Search Box--}}

    <div class="row">
        <div class="col-md-3"> {{--CS--}}
            <div class="box box-solid box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-user"></i>&nbsp;Customer Service</h3>
                    <div class="box-tools pull-right">
                        <small class="label label-danger">{{$intCount['cs']}}</small>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-striped">
                        @foreach($arrCs as $key => $cs)
                            <tr>
                                <td>{{$key}}</td>
                                <td>{{$cs->UserProfil->realname}}</td>
                                <td><small class="label label-info"><a href="/job/user/{{$cs->id}}?month={{$intMonth}}&year={{$intYear}}" style="color:#fff">{{$cs['count']}}</a></small></td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-3"> {{--Partner--}}
            <div class="box box-solid box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-briefcase"></i>&nbsp;Partner (Tenant)</h3>
                    <div class="box-tools pull-right">
                        <small class="label label-danger">{{$intCount['pr']}}</small>
                    </div>
                </div>
                <div class="box-body">
                <table class="table table-striped">
                    @foreach($arrPr as $key => $value)
                        @if ($value['count'] != 0)
                        <tr>
                            <td>{{$key}}</td>
                            <td>{{$value->email}}</td>
                            <td><small class="label label-info">{{$value['count']}}</small></td>
                        </tr>
                        @endif
                    @endforeach
                </table>
                </div>
            </div>
        </div>

        <div class="col-md-3"> {{--Data Entry--}}
            <div class="box box-solid box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-group"></i>&nbsp;Data Entry</h3>
                    <div class="box-tools pull-right">
                        <small class="label label-danger">{{$intCount['de']}}</small>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-striped">
                        @foreach($arrDe as $key => $de)
                            <tr>
                                <td>{{$key}}</td>
                                <td>{{$de->email}}</td>
                                <td><small class="label label-info"><a href="/job/user/{{$de->id}}?month={{$intMonth}}&year={{$intYear}}" style="color:#fff">{{$de['count']}}</a></small></td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-3"> {{--Sources--}}
            <div class="box box-solid box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-globe"></i>&nbsp;Sources</h3>
                    <div class="box-tools pull-right">
                        <small class="label label-danger">{{$intCount['sr']}}</small>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-striped">
                        @foreach($arrSr as $key => $sr)
                            <tr>
                                <td>{{$key}}</td>
                                <td>{{$sr->name}}</td>
                                <td><small class="label label-info">{{$sr['count']}}</small></td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
    <a href="/job/content" class="btn btn-danger btn-sm"><i class="fa fa-mail-reply"></i>&nbsp;Kembali</a>
@stop