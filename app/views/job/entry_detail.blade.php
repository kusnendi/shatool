@extends('layout.default')

@section('main')
    {{--Search Box
    <div class="row">
        <div class="col-md-6">
            <div class="callout callout-info">
                <form method="get">
                    <div class="row form-group">
                        <div class="col-md-3">
                            <input type="text" class="form-control" name="month" value="{{$intMonth}}" placeholder="Bulan" pattern="[0-9]{2}"/>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="year" value="{{$intYear}}" placeholder="Tahun" pattern="[0-9]{4}"/>
                        </div>
                        <div class="col-md-1">
                            <button type="submit" class="btn btn-info">Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    /Search Box--}}
    <h4>Detail Entry Content</h4>
    <div class="row">
        <div class="col-md-10">
            <table class="table table-bordered table-striped">
                <tr>
                    <th class="col-md-1">Code</th>
                    <th class="col-md-4">Judul</th>
                    <th class="col-md-3">Perusahaan</th>
                    <th class="col-md-2">Fungsi</th>
                    <th class="col-md-3">Posted</th>
                    <th class="col-md-2">Created by</th>
                </tr>
                @foreach($arrJobs as $job)
                <tr>
                    <td>{{$job->code}}</td>
                    <td>{{$job->title}}</td>
                    <td>{{ucwords($job->Company->name)}}</td>
                    <td>{{$job->Specialist->name}}</td>
                    <td>{{$job->created}}</td>
                    <td>{{$job->User->email}}</td>
                </tr>
                @endforeach
            </table>
            {{$arrJobs->appends(array_except(Input::query(), Paginator::getPageName()))->links()}}
        </div>
    </div>
	<a href="/job/user/{{$user_id}}?month={{$month}}&year={{$year}}" class="btn btn-danger btn-sm"><i class="fa fa-mail-reply"></i>&nbsp;Kembali</a>
@stop