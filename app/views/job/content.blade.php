@extends('layout.default')

@section('main')
    {{--Search Box--}}
    <div class="row">
        <div class="col-md-12">
            <div class="callout callout-info">
                <form method="get">
                    <div class="row form-group">
                        <div class="col-md-1">
                            <input type="text" class="form-control" name="month" value="{{$intMonth}}" placeholder="Bulan" pattern="[0-9]{2}"/>
                        </div>
                        <div class="col-md-2">
                            <input type="text" class="form-control" name="year" value="{{$intYear}}" placeholder="Tahun" pattern="[0-9]{4}"/>
                        </div>
                        <div class="col-md-1">
                            <button type="submit" class="btn btn-info">Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--/Search Box--}}

    <div class="row">
        <div class="col-md-3"> {{--New--}}
            <div class="small-box bg-teal">
                <div class="inner">
                    <h3>{{$arrCount['new']}}</h3>
                    <p>This Month</p>
                </div>
                <div class="icon">
                    <i class="ion ion-document"></i>
                </div>
                <a class="small-box-footer" href="/job/source?month={{$intMonth}}&year={{$intYear}}">Click here <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-md-3"> {{--Existing--}}
            <div class="small-box bg-teal">
                <div class="inner">
                    <h3>{{$arrCount['existing']}}</h3>
                    <p>Existing</p>
                </div>
                <div class="icon">
                    <i class="ion ion-document-text"></i>
                </div>
                <a class="small-box-footer" href="#" target="_parent">Click here <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-md-3"> {{--Active--}}
            <div class="small-box bg-teal">
                <div class="inner">
                    <h3>{{$arrCount['active']}}</h3>
                    <p>Active</p>
                </div>
                <div class="icon">
                    <i class="ion ion-checkmark"></i>
                </div>
                <a class="small-box-footer" href="#" target="_parent">Click here <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-md-3"> {{--Expired--}}
            <div class="small-box bg-teal">
                <div class="inner">
                    <h3>{{$arrCount['expired']}}</h3>
                    <p>Expired</p>
                </div>
                <div class="icon">
                    <i class="ion ion-close"></i>
                </div>
                <a class="small-box-footer" href="#" target="_parent">Click here <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
    </div>
@stop