@extends('layout.default')

@section('main')
    <h2 class="page-header">Jobs Management | List Job</h2>
    <div class="row">
        <div class="col-md-12">
            <form method="put" action="{{url('job/search')}}">
                <div class="row form-group">
                    {{-- Parameter Search --}}
                    <div class="col-md-4">
                        <select name="paramSearch" class="form-control">
                            @foreach ($arrSearch as $param => $value)
                                <option value="{{$param}}">{{$value}}</option>
                            @endforeach
                        </select>
                    </div>
                    {{-- Parameter Value --}}
                    <div class="col-md-8">
                        <input class="form-control" type="text" name="paramKey" placeholder="Tulis kata kunci disini" />
                    </div>
                </div>
            </form>
        </div>
    </div>
    @if ($jobs->count())
    <div class="row">
        <div class="col-md-12">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Code</th>
                    <th>Company</th>
                    <th>Job Title</th>
                    <th>Posted</th>
                    <th>Expired</th>
                </tr>
            </thead>
            <tbody>
                @foreach($jobs as $job)
                <tr>
                    <td>{{HTML::link('job/detail/'.$job->code,$job->code)}}</td>
                    <td>{{$job->Company->name}}</td>
                    <td>{{$job->title}}</td>
                    <td>{{$job->created}}</td>
                    <td>{{$job->expired}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{  $jobs->appends(array_except(Input::query(), Paginator::getPageName()))->links()  }}
        </div>
    </div>
    @else
    <div class="alert alert-danger">
        <p>There are no job founded, please come back later!</p>
    </div>
    @endif
@stop()