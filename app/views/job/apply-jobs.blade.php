@extends('layout.default')

@section('main')
    {{--Search Box--}}
    <?php $arrBln = array(1=>'Jan','Feb','Mar','Apr','Mei','Jun','Jul','Ags','Sep','Okt','Nov','Des'); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="callout callout-info">
                <form id="ApplyJob" method="post" action="/job/apply-jobs">
                    <div class="row form-group">
                        <div class="col-md-2 col-xs-2">
                            <select id="fromMonth" name="from" class="form-control" required>
                                <option value="0" selected>Dari</option>
                                @foreach($arrBln as $key => $value)
                                    <option value="{{$key}}">{{$value}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-2 col-xs-2">
                            <select id="endMonth" name="end" class="form-control" required>
                                <option value="0" selected>Sampai</option>
                                @foreach($arrBln as $key => $value)
                                    <option value="{{$key}}">{{$value}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-2 col-xs-2">
                            <select id="intYear" name="intYear" class="form-control" required>
                                <option value="0" selected>Tahun</option>
                                @for($t=2014;$t<=date('Y');$t++)
                                    <option value="{{$t}}">{{$t}}</option>
                                @endfor
                            </select>
                        </div>
                        <div class="col-md-1">
                            <button id="Btn" type="submit" class="btn btn-info">Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--/Search Box--}}

    <div class="row">
        <div class="col-md-12">
            <div style="margin-bottom: 10px; display: none">
                <div id="chartApplyJob">

                </div>
            </div>
        </div>
    </div>

    <div class="row">
    <div id="ApplyReport">
        <div class="col-md-6">
            <div class="row">
                <div class="col-xs-12">
                    <div id="LoaderApply" class="text-center" style="display: none">
                        <img src="/assets/images/ajax-loader.gif" />
                    </div>
                    <div id="WrapperApply" class="panel panel-primary" style="display: none; min-height: 293px">
                        <div class="panel-heading">
                            <h4 class="panel-title">Overview Apply Jobs</h4>
                        </div>
                        <div id="MainContent" class="panel-body">

                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div id="LoaderMaps" class="text-center" style="display: none">
                        <img src="/assets/images/ajax-loader.gif" />
                    </div>
                    <div id="WrapMapping" class="panel panel-danger" style="display: none; min-height: 293px">
                        <div class="panel-heading">
                            <h4 id="titleMapping" class="panel-title"></h4>
                        </div>
                        <div id="MainMapping" class="panel-body">

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">

        </div>
    </div>
    </div>
@stop