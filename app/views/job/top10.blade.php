@extends('layout.default')

@section('main')
    <form method="get" action="/job/top10">
        <div class="row form-group">
            <div class="col-md-2">
                <label>Category</label>
                {{Form::select('category',$arrCat,$intCat,array('class' => 'form-control input-sm'))}}
            </div>
            <div class="col-md-5">
                <label>From</label>
                {{Form::input('date','date1',$intDate,array('class' => 'form-control input-sm'))}}
            </div>
            <div class="col-md-5">
                <label>To</label>
                {{Form::input('date','date2',$intDate2,array('class' => 'form-control input-sm'))}}
            </div>
        </div>
        <button type="submit" class="btn btn-danger btn-sm">Submit</button>
    </form>
    @if($Jobs->count())
    <h3 class="page-header">Top 10 {{($intCat > 1) ? 'Specialist' : 'Company'}}</h3>
    <div class="row" style="margin-top: 20px">
        <div class="col-md-4">
            <table class="table table-bordered">
                <thead>
                @if($intCat == 1)
                    <tr>
                        <td class="col-md-1">No</td>
                        <th class="col-md-10">Company</th>
                        <th class="col-md-1" style="text-align: center">Apply</th>
                    </tr>
                @else
                    <tr>
                        <td class="col-md-1">No</td>
                        <th class="col-md-10">Specialist</th>
                        <th class="col-md-1" style="text-align: center">Apply</th>
                    </tr>
                @endif
                </thead>
                <tbody>
                <?php $i=1 ?>
                @foreach($Jobs as $Job)
                    <tr>
                    @if($intCat == 1)
                        <td>{{$i}}</td>
                        <td>{{$Job->Company->name}}</td>
                        <td style="text-align: center">{{$Job->jumlah}}</td>
                    @else
                        <td>{{$i}}</td>
                        <td>{{$Job->Specialist->name}}</td>
                        <td style="text-align: center">{{$Job->jumlah}}</td>
                    @endif
                    </tr>
                    <?php $i++; ?>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @endif
@stop