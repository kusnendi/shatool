@extends('layout.default')

@section('main')
    {{--Search Box--}}
    <div class="row">
        <div class="col-md-8">
            <div class="callout callout-info">
                <form method="get">
                    <div class="row form-group">
                        <div class="col-md-3">
                            <input type="text" class="form-control" name="month" value="{{$intMonth}}" placeholder="Bulan" pattern="[0-9]{2}"/>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="year" value="{{$intYear}}" placeholder="Tahun" pattern="[0-9]{4}"/>
                        </div>
                        <div class="col-md-1">
                            <button type="submit" class="btn btn-info">Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--/Search Box--}}

    <div class="row">
        <div class="col-md-8">
            <table class="table table-bordered table-striped">
                <tr>
                    <th class="col-md-1">Tanggal</th>
                    <th class="col-md-2 text-center">00:01 - 06:00</th>
                    <th class="col-md-2 text-center">06:01 - 14:00</th>
                    <th class="col-md-2 text-center">14:01 - 22:00</th>
                    <th class="col-md-2 text-center">22:01 - 24:00</th>
                    <th class="col-md-1 text-center">Jumlah</th>
                </tr>
                @foreach($arrData as $data)
                <tr>
                    <td>{{$data['tanggal']}}</td>
                    <td class="text-center">{{$data['pagi']}}</td>
                    <td class="text-center">{{$data['siang']}}</td>
                    <td class="text-center">{{$data['sore']}}</td>
                    <td class="text-center">{{$data['malam']}}</td>
                    <td class="text-center"><a href="/job/entry-detail/{{$data['uid']}}/{{$data['date']}}">{{$data['jumlah']}}</a></td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
	<a href="/job/source" class="btn btn-danger btn-sm"><i class="fa fa-mail-reply"></i>&nbsp;Kembali</a>
@stop