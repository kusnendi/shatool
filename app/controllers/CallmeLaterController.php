<?php
/**
 * Created by PhpStorm.
 * User: Adicipta
 * Date: 10/24/2014
 * Time: 8:14 PM
 */

class CallmeLaterController extends BaseController
{

    /* Array Flags */
    public function arrFlags()
    {
        $arrData = array(
            '0' => '<small class="label label-danger">Requested</small>',
            '1' => '<small class="label label-success">Completed</small>',
            '2' => '<small class="label label-info">CALLME01</small>',
            '3' => '<small class="label label-info">CALLME02</small>',
            '4' => '<small class="label label-info">CALLME03</small>',
            '5' => '<small class="label label-warning">Quarantine</small>'
        );
        return $arrData;
    }

    /* Array Flags */
    public function arrStatus()
    {
        $arrData = array(
            '1' => 'COMPLETED',
            '2' => 'CALLME01',
            '3' => 'CALLME02',
            '4' => 'CALLME03',
            '5' => 'QUARANTINE'
        );
        return $arrData;
    }

    // Get all list call me's
    public function getIndex($uid = null)
    {
        //store uid in session
        if ($uid != null){
            //store cs user_id
            Session::put('csid',$uid);
        }

        if (Input::has('msisdn')){
            $msisdn   = $this->replacePlusAndZero(Input::get('msisdn'));
            $cmLaters = CallmeLater::with('User')->where('hand_phone','=',$msisdn)
                //->where(DB::raw("date_format(created_at,'%Y-%m-%d %H:%i:%s')"),'>=',Input::get('start_date'))
                //->where(DB::raw("date_format(created_at,'%Y-%m-%d %H:%i:%s')"),'<=',Input::get('end_date'))
                ->orderBy('status','asc')->orderBy('created_at','asc')
                ->paginate(15);
        }
        else {
            $cmLaters = CallmeLater::with('User')->orderBy('status','asc')->orderBy('created_at','asc')->paginate(15);
        }

        //date_current
        $now     = date('Ymd');

        //array Appointments
        $arrAppt = CallmeLater::where('status','=',0)->where(DB::raw("date_format(callme_back,'%Y%m%d')"),'<=',$now)->first();

        if (count($cmLaters)){
            foreach ($cmLaters as $key => $values){
                $updated = User::find($values->followed);
                if (count($updated)){
                    $cmLaters[$key]['updated'] = $updated->email;
                } else {
                    $cmLaters[$key]['updated'] = '-';
                }

                if ($values->status == 1){
                    $cmLaters[$key]['class'] = 'green';
                } elseif ($values->status == 0 && date('Ymd',strtotime($values->callme_back)) <= $now){
                    $cmLaters[$key]['class'] = 'red';
                } else {
                    $cmLaters[$key]['class'] = '';
                }
            }
        }

        return View::make('callme_later.index', compact('cmLaters','arrAppt'))
            ->with('title','Appointment');
    }

    // Get list flag
    public function getLists($flag,$uid=null)
    {

    }

    // Add new appointment
    public function getAdd($cmid){
        $callme = LokerCm::find($cmid);

        $data = array(
            'cmid'   => $cmid,
            'status' => $this->arrStatus(),
            'callme' => $callme
        );
        return View::make('callme_later.add', $data)
            ->with('title','Appointment');

        /*
        echo '<pre>';
        var_dump($callme);
        */
    }

    public function postSave(){
        $appointment = new CallmeLater();
        $appointment->hand_phone    = Input::get('hand_phone');
        $appointment->callme_back   = Input::get('callme_back');
        $appointment->notes         = Input::get('notes');
        $appointment->cm_id         = Input::get('cm_id');
        $appointment->user_id       = Session::get('csid');
        $appointment->save();

        return Redirect::to('appointment');
        //var_dump(Input::all());
    }

    // Process Edit Call Me
    public function getEdit($id){
        $cmLater = CallmeLater::find($id);
        $callme  = LokerCm::find($cmLater->cm_id);
        $data['callme'] = $callme;
        $data['status'] = $this->arrStatus();
        $data['cm']     = $cmLater;

        return View::make('callme_later.edit',$data)
            ->with('title','Call Me');
    }

    // Process Update Call Me
    public function postUpdate(){
        $id = Input::get('id');

        $cmLater= CallmeLater::find($id);
        $cmLater->notes   = Input::get('notes');
        $cmLater->status  = '1';
        $cmLater->followed= Session::get('csid');
        $cmLater->save();

        $LogCall= new LogCall();
        $LogCall->msisdn = $cmLater->hand_phone;
        $LogCall->notes  = Input::get('notes');
        $LogCall->type   = 'outgoing call';
        $LogCall->code   = Input::get('flag');
        $LogCall->cm_id  = $cmLater->cm_id;
        $LogCall->user_id= Session::get('csid');
        $LogCall->save();

        $callme = LokerCm::find($cmLater->cm_id);
        $callme->flag  = Input::get('flag');
        $callme->save();

        return Redirect::to('appointment');
    }
}