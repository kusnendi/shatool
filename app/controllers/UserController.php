<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		$title = 'Users | List User';

        if (Input::has('msisdn')) {
            $msisdn = $this->replacePlusAndZero(Input::get('msisdn'));
            $users = User::where('sms_flag', '=', '1')
                ->where('hand_phone','=',$msisdn)
                ->where('role_id', '=', '4')
                /*
                ->whereNotIn('id', function ($query) {
                    $query->select('user_id')
                        ->from('user_profiles')
                        ->whereNotNull('firstname');
                })
                ->whereNotIn('hand_phone', function ($query) {
                    $query->select('msisdn')
                        ->from('log_calls')
                        ->where('type', '=', 'outgoing call');
                })
                */
                ->paginate(20);
        } else {
            $users = User::where('sms_flag', '=', '1')
                ->where('role_id', '=', '4')
                ->whereNotIn('id', function ($query) {
                    $query->select('user_id')
                        ->from('user_profiles')
                        ->whereNotNull('firstname');
                })
                    /*
                ->whereNotIn('hand_phone', function ($query) {
                    $query->select('msisdn')
                        ->from('log_calls')
                        ->where('type', '=', 'outgoing call');
                })
                */
                ->paginate(20);
        }

        return View::make('user.index', compact('title','users'));
	}

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getList()
    {
        $title = 'Users | List User';
        $date  = date('Ym');

        if (Input::has('msisdn')){
            $msisdn   = $this->replacePlusAndZero(Input::get('msisdn'));
            $users = User::where('sms_flag', '=', '1')
                ->where('hand_phone','=',$msisdn)
                ->where('role_id', '=', '4')
                ->whereIn('hand_phone', function ($query) {
                    $query->select('msisdn')
                        ->from('log_calls')
                        ->where('type', '=', 'outgoing call');
                })
                ->take(20);
        } else {
            $users = User::where('sms_flag', '=', '1')
                ->where('role_id', '=', '4')
                /*
                ->whereNotIn('id', function ($query) {
                    $query->select('user_id')
                        ->from('user_profiles')
                        ->whereNotNull('firstname');
                })
                */
                ->whereIn('hand_phone', function ($query) use ($date) {
                    $query->select('msisdn')
                        ->from('log_calls')
                        ->where('type', '=', 'outgoing call')
                        ->where(DB::raw("date_format(created,'%Y%m')"),$date);
                })
                ->take(20);
        }

        if ($users) {
            foreach ($users as $key => $value) {
                $logcall = LogCall::with('userProfil')->where('type', '=', 'outgoing call')
                    ->where('msisdn', '=', $value->hand_phone)
                    ->orderBy('id', 'DESC')
                    ->first();

                if ($logcall) {
                    $users[$key]['date'] = $logcall->created;
                    $users[$key]['cs']   = $logcall->userProfil->firstname;
                    //$users[$key]['cs']   = $logcall->user_id;
                }
            }
        }

        return View::make('user.list', compact('title','users'));
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postStore()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getShow($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getEdit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postUpdate($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postDestroy($id)
	{
		//
	}


}
