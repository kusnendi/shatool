<?php
/**
 * Created by PhpStorm.
 * User: nendi
 * Date: 29/09/14
 * Time: 22:13
 */

class JobController extends BaseController{

    /* Get All Jobs List */
    public function getIndex($param=null){
        $arrSearch = array(
            'title'     => 'Judul Lowongan',
            'code'      => 'Kode Lowongan',
            'created'   => 'Tanggal Posting',
            'expired'   => 'Tanggal Expired',
        );

        if (is_null($param))
        {
            $jobs = Job::with('Company')->paginate(20);
        }
        else
        {
            $paramSearch = Input::get('paramSearch');
            $paramKey    = Input::get('paramKey');

            if (empty($paramSearch) && empty($paramKey))
            {
                $jobs = Job::with('Company')->paginate(20);
            }
            else
            {
                $jobs = Job::with('Company')
                    ->where($paramSearch,'=',$paramKey)
                    ->paginate(20);
            }

        }

        return View::make('job.index', compact('jobs','arrSearch'))
            ->with('title','Jobs Management');
    }

    public function getSearch(){
        /* Set Array Search */
        $arrSearch = array(
            'title'     => 'Judul Lowongan',
            'code'      => 'Kode Lowongan',
            'created'   => 'Tanggal Posting',
            'expired'   => 'Tanggal Expired',
        );
        /* Set Parameter Search & Key */
        $paramSearch = Input::get('paramSearch');
        $paramKey    = Input::get('paramKey');

        if (empty($paramSearch) && empty($paramKey))
        {
            $jobs = Job::with('Company')->paginate(20);
        }
        else
        {
            $jobs = Job::with('Company')
                ->where($paramSearch,'like','%'.$paramKey.'%')
                ->paginate(20);
        }

        return View::make('job.index', compact('jobs','arrSearch'))
            ->with('title','Jobs Management');
    }

    public function getDetail($code){
        $job = Job::with('Company')
            ->with('City')
            ->where('code','=',$code)
            ->first();

        if ($job)
        {
            return View::make('job.detail', compact('job'))
                ->with('title','Jobs | Detail Job '.$code);
        }

        return View::make('404')
            ->with('title', 'Page Not Found');
    }

    public function getBulkupdate($min,$max){
        $jobs = Job::where('id','>=',$min)
            ->where('id','<=',$max)
            ->get();

        foreach ($jobs as $job){
            $smsContent = $job->sms_content;
            $kode = substr($smsContent,0,8);
            $link = '/'.substr($kode,1,6);
            $text = array($kode,$link);
            $Str  = array('['.$job->code.']','/'.$job->code);
            $sms  = str_replace($text,$Str,$smsContent);

            DB::table('jobs')
                ->where('code', $job->code)
                ->update(array('sms_content' => $sms));
        }
        return 'OK';
    }

    public function getTop10(){

        $arrCat   = array(1 => 'Company', 2 => 'Specialist');
        $category = Input::get('category');
        $date1    = date('Y-m-d H:i:s',strtotime(Input::get('date1')));
        $date2    = date('Y-m-d H:i:s',strtotime(Input::get('date2')));
        //$date1='2014-10-01 00:00:00';
        //$date1='2014-11-24 00:00:00';

        if (empty($category) || $category == 1)
        {
            if (!empty($date1) and !empty($date2))
            {
                $Jobs = ApplyJob::with('Company')
                    ->select('*', DB::raw('count(*) as jumlah'))
                    ->whereNotNull('company_id')
                    ->where('created','>=',$date1)
                    ->where('created','<=',$date2)
                    ->groupBy('company_id')
                    ->orderBy('jumlah','desc')
                    ->take(10)
                    ->get();
            }
            else
            {
                $Jobs = ApplyJob::with('Company')
                    ->select('*', DB::raw('count(*) as jumlah'))
                    ->whereNotNull('company_id')
                    ->groupBy('company_id')
                    ->orderBy('jumlah','desc')
                    ->take(10)
                    ->get();
            }
        }
        else
        {
            if (!empty($date1) and !empty($date2))
            {
                $Jobs = ApplyJob::with('Specialist')
                    ->select('*', DB::raw('count(*) as jumlah'))
                    ->whereNotNull('specialist_id')
                    ->where('created','>=',$date1)
                    ->where('created','<=',$date2)
                    ->groupBy('specialist_id')
                    ->orderBy('jumlah','desc')
                    ->take(10)
                    ->get();
            }
            else
            {
                $Jobs = ApplyJob::with('Specialist')
                    ->select('*', DB::raw('count(*) as jumlah'))
                    ->whereNotNull('specialist_id')
                    ->groupBy('specialist_id')
                    ->orderBy('jumlah','desc')
                    ->take(10)
                    ->get();
            }

        }

        $intCat   = (empty($category)) ? 1 : $category;
        $intDate  = (Input::get('date1')) ? Input::get('date1') : '';
        $intDate2 = (Input::get('date2')) ? Input::get('date2') : '';

        return View::make('job.top10',compact('intCat','Jobs','arrCat','intDate', 'intDate2'))
            ->with('title','Monitoring Top 10');

    }

    /** Content Overview */
    public function getContent(){
        /** intDate */
        $month = date('m');
        $year  = date('Y');
        $intMonth = (Input::has('month')) ? Input::get('month') : $month;
        $intYear  = (Input::has('year')) ? Input::get('year') : $year;
        $YM = $intYear.$intMonth;
        $Ymd= date('Ymd');

        /** array jobs count */
        $new = Job::where(DB::raw("date_format(created,'%Y%m')"),'=',$YM)->count();
        $existing = Job::where(DB::raw("date_format(created,'%Y%m')"),'<',$YM)->where(DB::raw("date_format(expired,'%Y%m%d')"),'>=',$Ymd)->count();
        $active   = Job::where(DB::raw("date_format(created,'%Y%m')"),'=',$YM)->where(DB::raw("date_format(expired,'%Y%m%d')"),'>=',$Ymd)->count();
        $expired  = Job::where(DB::raw("date_format(created,'%Y%m')"),'=',$YM)->where(DB::raw("date_format(expired,'%Y%m%d')"),'<',$Ymd)->count();
        $arrCount = array(
            'new'       => $new,
            'existing'  => $existing,
            'active'    => $active,
            'expired'   => $expired
        );

        return View::make('job.content',compact('arrCount','intMonth','intYear'))
            ->with('title','Content Overview');
    }

    /** Source Overview */
    public function getSource(){
        /** intDate */
        $month = date('m');
        $year  = date('Y');
        $intMonth = (Input::has('month')) ? Input::get('month') : $month;
        $intYear  = (Input::has('year')) ? Input::get('year') : $year;
        $YM = $intYear.$intMonth;
        //$Ymd= date('Ymd');

        /** array count source job  */
        //count jobs from customer service
        $cs = Job::where(DB::raw("date_format(created,'%Y%m')"),'=',$YM)
            ->whereIn('user_id', function ($query){
                $query->select('id')
                    ->from('users')
                    ->where('role_id','=','5')
                    ->whereNotIn('id',['185236','185237']);
            })
            ->count();
        //count jobs from data entry
        $de = Job::where(DB::raw("date_format(created,'%Y%m')"),'=',$YM)
            ->whereIn('user_id', function ($query){
                $query->select('id')
                    ->from('users')
                    ->where('role_id','=','5')
                    ->whereIn('id',['185236','185237']);
            })
            ->count();
        //count jobs from partner
        $pr = Job::where(DB::raw("date_format(created,'%Y%m')"),'=',$YM)
            ->whereIn('user_id', function ($query){
                $query->select('id')
                    ->from('users')
                    ->where('role_id','=','2');
                    //->whereIn('id',['5866','75']);
            })
            ->count();

        //count jobs from sources
        $sr = Job::where(DB::raw("date_format(created,'%Y%m')"),'=',$YM)
            ->where('source_id','!=',0)
            ->count();

        /** int count jobs */
        $intCount = array(
            'cs'    => $cs,
            'de'    => $de,
            'pr'    => $pr,
            'sr'    => $sr
        );

        /** array source content */
        /** @var $arrCs */
        $arrCs = User::with('UserProfil')->where('role_id','=',5)->whereNotIn('id',['185236','185237'])->orderBy('email')->get();
        foreach ($arrCs as $key => $value){
            $count = Job::where(DB::raw("date_format(created,'%Y%m')"),'=',$YM)->where('user_id','=',$value->id)->count();
            $arrCs[$key]['count'] = $count;
        }
        /** @var $arrDe */
        $arrDe = User::where('role_id','=',5)->whereIn('id',['185236','185237'])->get();
        foreach ($arrDe as $key => $value){
            $count = Job::where(DB::raw("date_format(created,'%Y%m')"),'=',$YM)->where('user_id','=',$value->id)->count();
            $arrDe[$key]['count'] = $count;

        }
        /** arrJobs from Partner */
        $arrPr = User::where('role_id','=',2)
                //->whereIn('id',['5866','75'])
                ->get();
        foreach ($arrPr as $key => $value){
            $count = Job::where(DB::raw("date_format(created,'%Y%m')"),'=',$YM)->where('user_id','=',$value->id)->count();
            $arrPr[$key]['count'] = $count;

        }
        /** arrJobs from Sources */
        $arrSr = Source::all();
        foreach ($arrSr as $key => $value){
            $count = Job::where(DB::raw("date_format(created,'%Y%m')"),'=',$YM)->where('source_id','=',$value->id)->count();
            $arrSr[$key]['count'] = $count;

        }
        /** debug
        echo '<pre>';
        echo print_r($arrCs);
        die();
         */
        return View::make('job.source', compact('intCount','intMonth','intYear','arrCs','arrDe','arrPr','arrSr'))
            ->with('title','Source Overview');
    }

    public function getUser($id){
        /** intDate */
        $month = date('m');
        $year  = date('Y');
        $intMonth = (Input::has('month')) ? Input::get('month') : $month;
        $intYear  = (Input::has('year')) ? Input::get('year') : $year;
        $YM = $intYear.$intMonth;
        //$Ymd= date('Ymd');

        /** array jobs by user created */
        $arrJob   = DB::select( DB::raw("SELECT user_id, date_format(created,'%d/%m/%Y') as tanggal, date_format(created,'%d%m%Y') as tgl, count(*) as jumlah FROM jobs WHERE user_id = ".$id."  and date_format(created,'%Y%m') = ".$YM." group by tanggal" ) );
        //break down counter to each time shift

        $arrData= array();
        foreach ($arrJob as $key => $value){
            $pagi = Job::where('user_id',$id)
                ->where(DB::raw("date_format(created,'%d%m%Y%H%i')"),'>=', $value->tgl . '0000')
                ->where(DB::raw("date_format(created,'%d%m%Y%H%i')"),'<', $value->tgl . '0600')
                ->count();
            $siang= Job::where('user_id',$id)
                ->where(DB::raw("date_format(created,'%d%m%Y%H%i')"),'>=', $value->tgl . '0601')
                ->where(DB::raw("date_format(created,'%d%m%Y%H%i')"),'<=', $value->tgl . '1400')
                ->count();
            $sore = Job::where('user_id',$id)
                ->where(DB::raw("date_format(created,'%d%m%Y%H%i')"),'>=', $value->tgl . '1401')
                ->where(DB::raw("date_format(created,'%d%m%Y%H%i')"),'<=', $value->tgl . '2200')
                ->count();
            $malam= Job::where('user_id',$id)
                ->where(DB::raw("date_format(created,'%d%m%Y%H%i')"),'>=', $value->tgl . '2201')
                ->where(DB::raw("date_format(created,'%d%m%Y%H%i')"),'<=', $value->tgl . '2400')
                ->count();
            $arrData[$key]['uid']       = $value->user_id;
            $arrData[$key]['date']      = $value->tgl;
            $arrData[$key]['tanggal']   = $value->tanggal;
            $arrData[$key]['pagi']      = $pagi;
            $arrData[$key]['siang']     = $siang;
            $arrData[$key]['sore']      = $sore;
            $arrData[$key]['malam']     = $malam;
            $arrData[$key]['jumlah']    = $value->jumlah;
        }

        return View::make('job.user', compact('intMonth','intYear','arrData'))
            ->with('title','User Creator');
    }

    /** View Job Entry Detail */
    public function getEntryDetail($id,$date){
        //get arrJobs
        $arrJobs = Job::with('Company')->with('User')->with('Specialist')->where('user_id',$id)
            ->where(DB::raw("date_format(created,'%d%m%Y')"),'=', $date)
            ->paginate(15);

        $user_id = $id;
        $month   = substr($date,2,2);
        $year    = substr($date,4,4);

        return View::make('job.entry_detail',compact('arrJobs','user_id','month','year'))
            ->with('title','Detail User Entry');
    }

    /** View Apply Jobs */
    public function getApplyJobs(){
        /** intDate */
        $month = date('m');
        $year  = date('Y');
        $intMonth = (Input::has('month')) ? Input::get('month') : $month;
        $intYear  = (Input::has('year')) ? Input::get('year') : $year;
        $YM = $intYear.$intMonth;

        //get selected apply jobs
        return View::make('job.apply-jobs',compact('intMonth','intYear'))
            ->with('title','Apply Jobs Overview');
    }

    public function postApplyJobs(){
        //var date data's
        $fromMonth = sprintf('%02s',Input::get('from'));
        $endMonth  = sprintf('%02s',Input::get('end'));
        $intYear   = Input::get('intYear');
        //data
        $arrApplys = ApplyJob::select(DB::raw("date_format(created,'%m-%Y') as tanggal"),DB::raw("date_format(created,'%Y%m') as intDate"),DB::raw("count(*) as jumlah"))
                    ->whereIn('user_id',function($query){
                        $query->select('id')
                            ->from('users')
                            ->where('role_id',4);
                    })
                    ->where(DB::raw("date_format(created,'%Y%m')"),'>=',$intYear.$fromMonth)
                    ->where(DB::raw("date_format(created,'%Y%m')"),'<=',$intYear.$endMonth)
                    ->groupBy('tanggal')
                    ->get();
        $Specialists= ApplyJob::select(DB::raw("count(*) as jumlah"),'specialist_id')
            ->whereIn('user_id',function($query){
                $query->select('id')
                    ->from('users')
                    ->where('role_id',4);
            })
            ->where(DB::raw("date_format(created,'%Y%m')"),'>=',$intYear.$fromMonth)
            ->where(DB::raw("date_format(created,'%Y%m')"),'<=',$intYear.$endMonth)
            ->orderBy('jumlah','desc')
            ->take(10)
            ->groupBy('specialist_id')
            ->get();
        $Cities     = ApplyJob::select(DB::raw("count(*) as jumlah"),'city_id','name')
            ->leftJoin('jobs', function($join) {
                $join->on('apply_jobs.job_id', '=', 'jobs.id');
            })
            ->leftJoin('cities', function($join) {
                $join->on('jobs.city_id', '=', 'cities.id');
            })
            ->whereIn('apply_jobs.user_id',function($query){
                $query->select('id')
                    ->from('users')
                    ->where('role_id',4);
            })
            ->where(DB::raw("date_format(apply_jobs.created,'%Y%m')"),'>=',$intYear.$fromMonth)
            ->where(DB::raw("date_format(apply_jobs.created,'%Y%m')"),'<=',$intYear.$endMonth)
            ->orderBy('jumlah','desc')
            ->take(10)
            ->groupBy('city_id')
            ->get();
        $Educations= ApplyJob::select(DB::raw("count(*) as jumlah"),'last_education')
            ->leftJoin('user_profiles', function($join) {
                $join->on('apply_jobs.user_id', '=', 'user_profiles.user_id');
            })
            ->whereIn('apply_jobs.user_id',function($query){
                $query->select('id')
                    ->from('users')
                    ->where('role_id',4);
            })
            ->where(DB::raw("date_format(apply_jobs.created,'%Y%m')"),'>=',$intYear.$fromMonth)
            ->where(DB::raw("date_format(apply_jobs.created,'%Y%m')"),'<=',$intYear.$endMonth)
            ->orderBy('jumlah','desc')
            ->groupBy('last_education')
            ->take(5)
            ->get();


        $arrBln    = array(1=>'Jan','Feb','Mar','Apr','Mei','Jun','Jul','Ags','Sep','Okt','Nov','Des');
        $arrData = array();
        $arrData[] = array('Bulan','SMS','WEB');

        foreach ($arrApplys as $key => $value){
            $i = (int)substr($value->tanggal,0,2);
            $sms = ApplyJob::where('flag',0)
                ->whereIn('user_id',function($query){
                    $query->select('id')
                        ->from('users')
                        ->where('role_id',4);
                })
                ->where(DB::raw("date_format(created,'%m-%Y')"),$value->tanggal)
                ->count();
            $web = ApplyJob::where('flag',1)
                ->whereIn('user_id',function($query){
                    $query->select('id')
                        ->from('users')
                        ->where('role_id',4);
                })
                ->where(DB::raw("date_format(created,'%m-%Y')"),$value->tanggal)
                ->count();
            $cv  = UserProfile::whereNotNull('firstname')->whereNotNull('lastname')
                ->whereNotNull('gender')
                ->whereIn('user_id',function($query){
                    $query->select('id')
                        ->from('users')
                        ->where('role_id',4);
                })
                ->where(DB::raw("date_format(created,'%m-%Y')"),$value->tanggal)
                ->groupBy('user_id')
                ->get();
            $allcv  = UserProfile::select(DB::raw("user_id"))
                ->whereNotNull('firstname')->whereNotNull('lastname')
                ->whereNotNull('gender')
                ->whereIn('user_id',function($query){
                    $query->select('id')
                        ->from('users')
                        ->where('role_id',4)
                        ->where('source',0);
                })
                ->where(DB::raw("date_format(created,'%Y%m')"),'<=',$value->intDate)
                ->groupBy('user_id')
                ->get();

            $arrApplys[$key]['sms']     = $sms;
            $arrApplys[$key]['web']     = $web;
            $arrApplys[$key]['cv']      = $cv;
            $arrApplys[$key]['totalcv'] = $allcv;

            //store array sms & web
            //$arrData[] = array($arrBln[$i],$sms,$web);
        }

        /*json data to process view
        $jsonData = json_encode($arrData);
        return array(
            'status' => true,
            'arrGraph'  => $jsonData
        );
        */
        return View::make('job.ajax-apply-jobs',compact('arrApplys','Specialists','Cities','Educations'));
    }

    public function postMappingApply(){
        //Var User Data...
        $intDate = Input::get('tgl');

        $Maps    = UserProfile::select(DB::raw("count(*) as jumlah"),DB::raw("date_format(created,'%Y-%m') as tanggal"))
                ->whereNotNull('firstname')
                ->whereNotNull('lastname')
                ->whereIn('user_id',function($query) use ($intDate){
                    $query->select('user_id')
                        ->from('apply_jobs')
                        ->where(DB::raw("date_format(created,'%m-%Y')"),$intDate);
                })
                ->groupBy('tanggal')
                ->get();
        foreach ($Maps as $key => $value){
            $cv  = UserProfile::whereNotNull('firstname')->whereNotNull('lastname')
                ->whereNotNull('gender')
                ->whereIn('user_id',function($query){
                    $query->select('id')
                        ->from('users')
                        ->where('role_id',4);
                })
                ->where(DB::raw("date_format(created,'%Y-%m')"),$value->tanggal)
                ->groupBy('user_id')
                ->get();
            $isReg=User::whereIn('id',function($query) use ($value){
                $query->select('user_id')
                    ->from('user_profiles')
                    ->whereNotNull('firstname')
                    ->whereNotNull('lastname')
                    ->where(DB::raw("date_format(created,'%Y-%m')"),'=',$value->tanggal);
                })
                ->where('role_id',4)
                ->where('sms_flag',1)
                ->count();
            $isEmail=User::whereIn('id',function($query) use ($value){
                $query->select('user_id')
                    ->from('user_profiles')
                    ->whereNotNull('firstname')
                    ->whereNotNull('lastname')
                    ->whereNotNull('email')
                    ->where('email','!=','')
                    ->where(DB::raw("date_format(created,'%Y-%m')"),'=',$value->tanggal);
                })
                ->where('role_id',4)
                ->where('sms_flag',0)
                ->count();

            $Maps[$key]['cv']     = $cv;
            $Maps[$key]['isReg']  = $isReg;
            $Maps[$key]['isEmail']=$isEmail;
        }
        //$Maps->to();
        return View::make('job.ajax-mapping-apply',compact('Maps'));
    }

    public function getJsonData(){
        $arrData = array();
        $arrData[] = array('Bulan','SMS','WEB');
        $arrData[] = array('Jan',350,540);
        $arrData[] = array('Feb',250,440);
        $arrData[] = array('Mar',150,640);

        echo json_encode($arrData);
    }
}