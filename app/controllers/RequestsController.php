<?php
/**
 * Created by PhpStorm.
 * User: Adicipta
 * Date: 10/24/2014
 * Time: 8:14 PM
 */

class RequestsController extends BaseController
{
    /** Requests
     * Handler*/
    public function getIndex(){
        $msisdn = Input::get('msisdn');
        $content= Input::get('content');
        $trx_id = Input::get('trx_id');

        if (isset($msisdn)){
            $data = array(
                'msisdn'    => $msisdn,
                'content'   => $content,
                'trx_id'    => $trx_id,
                'response'  => 1
            );

            return $data;
        } else {
            return 504;
        }
    }

    public function getResource(){
        $mtrUrl  = 'http://localhost:8000/requests?msisdn=085798535250&content=nendi&trx_id=23120012345';
        $request = \Httpful\Request::get($mtrUrl)->send();

        echo '<pre>';
        echo print_r($request);
    }

    public function getUpdate(){
        $users = DB::table('profile')->select('id','hand_phone')->get();

        $ids = array();
        $sql = "UPDATE profile SET hand_phone = CASE id ";
        foreach ($users as $user){
            $new_hp= $user->hand_phone+1;
            $ids[] = $user->id;
            $sql  .= sprintf("WHEN %d THEN '%s' ",$user->id,$new_hp);
        }
        $idss = implode(',', $ids);
        $sql .= "END WHERE id IN (".$idss.")";

        $exec = DB::statement($sql);

        if ($exec) {
            return 'OK';
        } else {
            return 'NOK';
        }
        //echo '<pre>';
        //echo $sql;
    }

}