<?php

class CompanyController extends \BaseController {
    /* Index Page */
    public function getIndex(){
        $companies = Company::with('Jobs')->paginate(20);

        return View::make('company.index',compact('companies'))
            ->with('title','Companies | List Company');
    }

    /* Top 10 Company */
    public function getTop10(){
        //
    }

    /* Company Jobs List */
    public function getListjobs($id){
        $company    = Company::find($id)->first();

        if ($company->count())
        {
            $jobs   = Job::whereCompany_id($id)->paginate(20);

            return View::make('company.listJobs',compact('jobs'))
                ->with('title','Companies | Jobs Management');
        }
        else{
            return View::make('hello');
        }

    }
} 