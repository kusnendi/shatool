<?php
/**
 * Created by PhpStorm.
 * User: Adicipta
 * Date: 10/24/2014
 * Time: 8:14 PM
 */

class LokercmController extends BaseController
{

    /* Array Flags */
    public function arrFlags()
    {
        $arrData = array(
            '0' => '<small class="label label-danger">Requested</small>',
            '1' => '<small class="label label-success">Completed</small>',
            '2' => '<small class="label label-info">CALLME01</small>',
            '3' => '<small class="label label-info">CALLME02</small>',
            '4' => '<small class="label label-info">CALLME03</small>'
        );
        return $arrData;
    }

    /* Array Flags */
    public function arrStatus()
    {
        $arrData = array(
            '1' => 'COMPLETED',
            '2' => 'CALLME01',
            '3' => 'CALLME02',
            '4' => 'CALLME03'
        );
        return $arrData;
    }

    /* Array Gender */
    public function arrGender()
    {
        $arrData = array(
            '1' => 'Laki-Laki',
            '2' => 'Perempuan'
        );
        return $arrData;
    }

    /* Array Careers */
    public function arrCareer()
    {
        $arrData = array(
            '1' => 'Fresh Graduate/Pemula',
            '2' => 'Supervisor',
            '3' => 'Asisten Manager',
            '4' => 'General Manager/Direktur/CEO',
            '5' => 'Staf/Senior Staf'
        );
        return $arrData;
    }

    /* Array Martial */
    public function arrMartila()
    {
        $arrData = array(
            '1' => 'Lajang',
            '2' => 'Menikah',
            '3' => 'Cerai'
        );
        return $arrData;
    }

    /* Array Education */
    public function arrEducation()
    {
        $arrData = array(
            'SMA'   => 'SMA',
            'SMK'   => 'SMK',
            'D1'    => 'D1',
            'D2'    => 'D2',
            'D3'    => 'D3',
            'D4'    => 'D4',
            'S1'    => 'S1',
            'S2'    => 'S2',
            'S3'    => 'S3'
        );
        return $arrData;
    }

    /* Array Action CallMe */
    public function arrActionCM(){
        $arrData = array(
            '0' => '',
            '1' => '',
            '2' => ''
        );
    }

    // Get all list call me's
    public function getIndex($uid = null)
    {
        //store uid in session
        if ($uid != null){
            //store cs user_id
            Session::put('csid',$uid);
        }

        //date_current
        $now     = date('Ymd');

        //check arrSearch
        if (Input::has('msisdn')){
            $msisdn  = $this->replacePlusAndZero(Input::get('msisdn'));
            $callmes = LokerCm::with('User')->where('hand_phone','=',$msisdn)->paginate(10);
        } else{
            $callmes = LokerCm::with('User')->whereFlag(0)->paginate(10);
        }

        //array Appointments
        $arrAppt = CallmeLater::where('status','=',0)->where(DB::raw("date_format(callme_back,'%Y%m%d')"),'<=',$now)->first();

        /*
        if ($callmes) {
            foreach ($callmes as $key => $value){
                $callme1    = LogCall::with('userProfil')->where('cm_id','=',$value->id)->where('code','=','CALLME01')->get();
                $callme2    = LogCall::with('userProfil')->where('cm_id','=',$value->id)->where('code','=','CALLME02')->get();
                $callme3    = LogCall::with('userProfil')->where('cm_id','=',$value->id)->where('code','=','CALLME03')->get();
                $complete   = LogCall::with('userProfil')->where('cm_id','=',$value->id)->where('code','=','OUT016')->get();
                $arrUser    = User::with('UserCertificates')->where('hand_phone',$value->hand_phone)->first();
                $isUpload   = $arrUser->UserCertificates->count();
                $callmes[$key]['callme1'] = $callme1;
                $callmes[$key]['callme2'] = $callme2;
                $callmes[$key]['callme3'] = $callme3;
                $callmes[$key]['complete']= $complete;
                $callmes[$key]['isUpload']= $isUpload;
            }
        }
        */

        $status = $this->arrFlags();
        $count  = array(
            'completed' => LokerCm::whereFlag(1)->count(),
            'callme1'   => LokerCm::whereFlag(2)->count(),
            'callme2'   => LokerCm::whereFlag(3)->count(),
            'callme3'   => LokerCm::whereFlag(4)->where(DB::raw("now()"),'<',DB::raw("date_add(created_at, interval 14 day)"))->count(),
            'karantina' => LokerCm::whereFlag(4)->where(DB::raw("now()"),'>=',DB::raw("date_add(created_at, interval 14 day)"))->count(),
            //'pending'   => LokerCm::whereRaw('flag = ? or flag = ? or flag = ?', array(2,3,4))->count(),
            'requested' => LokerCm::whereFlag(0)->count()
        );

        return View::make('lokercm.index', compact('callmes', 'status', 'count', 'arrAppt'))
            ->with('title', 'Monitoring Call Me');
    }

    // Get list flag
    public function getLists($flag,$uid=null)
    {
        //store uid in session
        if ($uid != null){
            //store cs user_id
            Session::put('csid',$uid);
        }

        //check arrSearch
        if (Input::has('msisdn')){
            $msisdn  = $this->replacePlusAndZero(Input::get('msisdn'));
            $callmes = LokerCm::with('User')->where('hand_phone','=',$msisdn)->paginate(10);
        }
        elseif ($flag == 4) {
            $callmes = LokerCm::with('User')->whereFlag($flag)->where(DB::raw("now()"),'<',DB::raw("date_add(created_at, interval 14 day)"))->paginate(10);
        }
        elseif ($flag == 5) {
            $callmes = LokerCm::with('User')->whereFlag(4)->where(DB::raw("now()"),'>=',DB::raw("date_add(created_at, interval 14 day)"))->paginate(10);
        } else {
            $callmes = LokerCm::with('User')->whereFlag($flag)->paginate(10);
        }

        //date_current
        $now     = date('Ymd');

        //array Appointments
        $arrAppt = CallmeLater::where('status','=',0)->where(DB::raw("date_format(callme_back,'%Y%m%d')"),'<=',$now)->first();

        if ($callmes) {
            foreach ($callmes as $key => $value){
                $callme1    = LogCall::with('userProfil')->where('cm_id','=',$value->id)->where('code','=','CALLME01')->get();
                $callme2    = LogCall::with('userProfil')->where('cm_id','=',$value->id)->where('code','=','CALLME02')->get();
                $callme3    = LogCall::with('userProfil')->where('cm_id','=',$value->id)->where('code','=','CALLME03')->get();
                $complete   = LogCall::with('userProfil')->where('cm_id','=',$value->id)->where('code','=','OUT016')->get();
                $arrUser    = User::with('UserCertificates')->where('hand_phone',$value->hand_phone)->first();
                $isUpload   = $arrUser->UserCertificates->count();
                $callmes[$key]['callme1'] = $callme1;
                $callmes[$key]['callme2'] = $callme2;
                $callmes[$key]['callme3'] = $callme3;
                $callmes[$key]['complete']= $complete;
                $callmes[$key]['isUpload']= $isUpload;
            }
        }

        $status = $this->arrFlags();
        $count  = array(
            'completed' => LokerCm::whereFlag(1)->count(),
            'callme1'   => LokerCm::whereFlag(2)->count(),
            'callme2'   => LokerCm::whereFlag(3)->count(),
            'callme3'   => LokerCm::whereFlag(4)->where(DB::raw("now()"),'<',DB::raw("date_add(created_at, interval 14 day)"))->count(),
            'karantina' => LokerCm::whereFlag(4)->where(DB::raw("now()"),'>=',DB::raw("date_add(created_at, interval 14 day)"))->count(),
            //'pending'   => LokerCm::whereRaw('flag = ? or flag = ? or flag = ?', array(2,3,4))->count(),
            'requested' => LokerCm::whereFlag(0)->count()
        );

        return View::make('lokercm.index', compact('callmes', 'status', 'count', 'arrAppt'))
            ->with('title', 'Monitoring Call Me');
    }

    // Process Follow Up CM
    public function getProcess($id, $msisdn)
    {
        $callMe = LokerCm::find($id);

        if ($callMe->count()) {
            // Check User from table users
            $User = User::whereHand_phone($msisdn)->first();

            if (isset($User)) { // (member)
                $user_id = $User->id;
                // return to curriculum
                return Redirect::to('callme/edit/' . $id)
                    ->with('msisdn',$callMe->hand_phone);
            } else { // (not member)
                // return to create user
                return Redirect::to('callme/createuser/'.$callMe->id);
            }

        } else {
            return Redirect::to('callme');
        }
    }

    // Process Log Calls Call Me
    public function getLogcalls($code){
        // Cek Existed Queue Call Me
        $callMe = LokerCm::whereCode($code)->first();
        if ($callMe->count()) {
            // return to update log calls
            return Redirect::to('callme/logcalls')
                ->with('msisdn',$callMe->hand_phone);
        } else {
            return Redirect::to('callme');
        }
    }

    // Process Create New User
    public function getCreateuser($id)
    {
        // Cek Pelanggan dari Loker CM
        $callme = LokerCm::find($id);

        if ($callme->count()){
            $password = 'ef6313f26174ed7133976daea15edb73df0bb1e6';

            $user = new User();
            $user->code = Str::random(6);
            $user->hand_phone = $callme->hand_phone;
            $user->password = $password;
            $user->is_approved = 1;
            $user->role_id = 4;
            $user->created = date('Y-m-d H:i:s');
            $user->save();

            return Redirect::to('callme/edit/'.$callme->code)
                ->with('password','1234')
                ->with('msisdn',$callme->hand_phone);
        } else {
            return Redirect::to('callme');
        }
    }

    // Process Edit Call Me
    public function getEdit($id){
        $callme = LokerCm::find($id);
        $data['callme'] = $callme;
        $data['status'] = $this->arrStatus();

        return View::make('lokercm.edit',$data)
            ->with('title','Call Me');
    }

    // Process Update Call Me
    public function postUpdate($id){
        $callme = LokerCm::find($id);
        $callme->hand_phone = Input::get('hand_phone');
        $callme->flag  = Input::get('flag');
        $callme->notes = Input::get('notes');
        $callme->save();

        switch (Input::get('flag')){
            case 1 : $code = 'OUT016'; break;
            case 2 : $code = 'CALLME01'; break;
            case 3 : $code = 'CALLME02'; break;
            case 4 : $code = 'CALLME03'; break;
        }

        //Create new entry
        $csid = Session::get('csid');
        DB::table('log_calls')->insert(array(
            'msisdn'        => Input::get('hand_phone'),
            'notes'         => Input::get('notes'),
            'type'          => 'outgoing call',
            'code'          => $code,
            'cm_id'         => $id,
            'user_id'       => $csid,
            'created'       => date('Y-m-d H:i:s')
        ));

        $appointment = Input::get('add_appointment');
        if (isset($appointment) && $appointment == 1){
            return Redirect::to('/appointment/add/'.$id);
        }
        else {
            return Redirect::to('callme');
        }

        //return Redirect::to('callme');
    }

    // Process Quarantine CM3
    public function getQuarantine($id){
        $arrCM = LokerCm::find($id);
        $arrCM->flag = '5';
        $arrCM->save();

        return Redirect::to('callme/lists/4');
    }

    //Process Blast Reminder Customer
    public function getReminder($id){
        $arrUser   = LokerCm::with('User')->where('id', '=', $id)->where('is_blast','=',1)->first();
        $tid       = '0';
        $smsContent= 'Anda telah menggunakan fitur LOKER CM pada ';
        //$request = \Httpful\Request::get($uri)->send();

        if ($arrUser){
            $tglFolUp    = date('d/m/Y H:i',strtotime($arrUser->created_at));

            $smsContent .= $tglFolUp . '.CS kami telah hub Anda,tp tdk berhasil. CS kami akan menghubungi Anda kembali utk pembuatan CV Anda';
            //Check User Subscription
            if ($arrUser->User->sms_flag){
                $strUrl     = 'http://202.43.162.235/responses/resetpasswordconfirmation';
                $strParam   = 'sms=' . urlencode($smsContent) . '&user_id=' . $arrUser->User->id . '&tid=' . $tid;
            } else {
                $strUrl     = 'http://202.43.162.235/responses/sendcodeverification';
                $strParam   = 'sms=' . urlencode($smsContent) . '&msisdn=' . $arrUser->hand_phone;
            }
            //Send SMS Notification
            $mtRequest  = $strUrl.'?'.$strParam;
            $response   = \Httpful\Request::get($mtRequest)->send();

            //Update Flag Reminder
            /*
            DB::table('loker_cms')->where('id', $arrUser->id)
                ->update(array('is_blast' => 0));
            */
            $updateCM = LokerCm::find($id);
            $updateCM->is_blast = 1;

            if ($updateCM->save()){
                return Redirect::to('/callme/lists/2')->with('msg','Reminder berhasil di kirim !');
            } else {
                return Redirect::to('/callme/lists/2')->with('msg','Reminder gagal di kirim !');
            }
        }
        else
        {
            return Redirect::to('/callme/lists/2')->with('msg','Reminder gagal di kirim !');
        }
    }

    //Report Daily Call Me
    public function getOverview(){
        $date_now = date('Ymd');

        //int counter call me overview
        $intNew = array(
            'requested' => LokerCm::where(DB::raw("date_format(created_at,'%Y%m%d')"),'=',$date_now)->count(),
            'current'   => LokerCm::where(DB::raw("date_format(created_at,'%Y%m%d')"),'=',$date_now)->where('flag','=',0)->count()
        );
        $intCM1 = array(
            'followup'  => LogCall::where(DB::raw("date_format(created,'%Y%m%d')"),'=',$date_now)->where('code','=','CALLME01')->count(),
            'current'   => LokerCm::where('flag','=',2)->count()
        );
        $intCM2 = array(
            'followup'  => LogCall::where(DB::raw("date_format(created,'%Y%m%d')"),'=',$date_now)->where('code','=','CALLME02')->count(),
            'current'   => LokerCm::where('flag','=',3)->count()
        );
        $intCM3 = array(
            'followup'  => LogCall::where(DB::raw("date_format(created,'%Y%m%d')"),'=',$date_now)->where('code','=','CALLME03')->count(),
            'current'   => LokerCm::whereFlag(4)->where(DB::raw("now()"),'<',DB::raw("date_add(created_at, interval 14 day)"))->count()
        );
        $intQrt = array(
            'current'   => LokerCm::whereFlag(4)->where(DB::raw("now()"),'>=',DB::raw("date_add(created_at, interval 14 day)"))->count()
        );
        $intCom = array(
            'current'   => LogCall::where(DB::raw("date_format(created,'%Y%m%d')"),'=',$date_now)->where('code','=','OUT016')->count()
        );

        //array Customer Service
        $arrCs = User::with('UserProfil')->where('role_id','=',5)->whereNotIn('id',['4541','185236','185237'])->orderBy('email')->get();
        foreach ($arrCs as $key => $value){
            //$count = Job::where(DB::raw("date_format(created,'%Y%m')"),'=',$YM)->where('user_id','=',$value->id)->count();
            $cm1    = LogCall::where('code','=','CALLME01')->where(DB::raw("date_format(created,'%Y%m%d')"),'=', $date_now)->where('user_id','=',$value->id)->count();
            $cm2    = LogCall::where('code','=','CALLME02')->where(DB::raw("date_format(created,'%Y%m%d')"),'=', $date_now)->where('user_id','=',$value->id)->count();
            $cm3    = LogCall::where('code','=','CALLME03')->where(DB::raw("date_format(created,'%Y%m%d')"),'=', $date_now)->where('user_id','=',$value->id)->count();
            $com    = LogCall::where('code','=','OUT016')->where(DB::raw("date_format(created,'%Y%m%d')"),'=', $date_now)->where('user_id','=',$value->id)->count();

            $arrCs[$key]['cm1'] = $cm1;
            $arrCs[$key]['cm2'] = $cm2;
            $arrCs[$key]['cm3'] = $cm3;
            $arrCs[$key]['com'] = $com;
        }

        return View::make('lokercm.overview',compact('arrCs','intNew','intCM1','intCM2','intCM3','intQrt','intCom'))
            ->with('title','Report Call Me');
    }

    //Report & Analisis Call Me
    public function getReport(){
        //date configuration
        $intMonth = (Input::has('month')) ? Input::get('month') : date('m');
        $intYear  = (Input::has('year'))  ? Input::get('year')  : date('Y');
        $date_now = date('Ymd');

        // Get % Complete Call Me
        $query  = "select count(*) as jumlah,(select code from log_calls where code != 'OUT016' and log_calls.cm_id=loker_cms.id order by code desc limit 1) as stat from loker_cms where flag = 1 and date_format(updated_at,'%Y%m') = ".$intYear.$intMonth." group by stat";
        $arrData= DB::select( DB::raw($query) );

        $totCom = 0;
        foreach ($arrData as $value){
            $totCom = $totCom + $value->jumlah;
        }

        //echo print_r($arrData);
        //die();
        //int counter call me overview
        $intNew = array(
            'requested' => LokerCm::where(DB::raw("date_format(created_at,'%Y%m')"),'=',$intYear.$intMonth)->count(),
            'current'   => LokerCm::where(DB::raw("date_format(created_at,'%Y%m%d')"),'=',$date_now)->where('flag','=',0)->count(),
            'all'       => LokerCm::count()
        );
        $intCM1 = array(
            'followup'  => LogCall::where(DB::raw("date_format(created,'%Y%m')"),'=',$intYear.$intMonth)->where('code','=','CALLME01')->count(),
            'current'   => LokerCm::where('flag','=',2)->count()
        );
        $intCM2 = array(
            'followup'  => LogCall::where(DB::raw("date_format(created,'%Y%m')"),'=',$intYear.$intMonth)->where('code','=','CALLME02')->count(),
            'current'   => LokerCm::where('flag','=',3)->count()
        );
        $intCM3 = array(
            'followup'  => LogCall::where(DB::raw("date_format(created,'%Y%m')"),'=',$intYear.$intMonth)->where('code','=','CALLME03')->count(),
            'current'   => LokerCm::whereFlag(4)->where(DB::raw("now()"),'<',DB::raw("date_add(created_at, interval 14 day)"))->count()
        );
        $intQrt = array(
            'current'   => LokerCm::whereFlag(4)->where(DB::raw("date_format(created_at,'%Y%m')"),'=',$intYear.$intMonth)->where(DB::raw("now()"),'>=',DB::raw("date_add(created_at, interval 14 day)"))->count()
        );
        $intCom = array(
            'current'   => LogCall::where(DB::raw("date_format(created,'%Y%m')"),'=',$intYear.$intMonth)->where('code','=','OUT016')->count()
        );

        //array Customer Service
        $arrCs = User::with('UserProfil')->where('role_id','=',5)->whereNotIn('id',['4541','185236','185237'])->orderBy('email')->get();
        foreach ($arrCs as $key => $value){
            //$count = Job::where(DB::raw("date_format(created,'%Y%m')"),'=',$YM)->where('user_id','=',$value->id)->count();
            $cm1    = LogCall::where('code','=','CALLME01')->where(DB::raw("date_format(created,'%Y%m')"),'=', $intYear.$intMonth)->where('user_id','=',$value->id)->count();
            $cm2    = LogCall::where('code','=','CALLME02')->where(DB::raw("date_format(created,'%Y%m')"),'=', $intYear.$intMonth)->where('user_id','=',$value->id)->count();
            $cm3    = LogCall::where('code','=','CALLME03')->where(DB::raw("date_format(created,'%Y%m')"),'=', $intYear.$intMonth)->where('user_id','=',$value->id)->count();
            $com    = LogCall::where('code','=','OUT016')->where(DB::raw("date_format(created,'%Y%m')"),'=', $intYear.$intMonth)->where('user_id','=',$value->id)->count();

            $arrCs[$key]['cm1'] = $cm1;
            $arrCs[$key]['cm2'] = $cm2;
            $arrCs[$key]['cm3'] = $cm3;
            $arrCs[$key]['com'] = $com;
        }

        //array call me requests
        $arrCM    = LokerCm::select(DB::raw("count(*) as jumlah"),DB::raw("date_format(created_at,'%d-%m-%Y') as tanggal"))
                    ->where(DB::raw("date_format(created_at,'%Y%m')"),'=', $intYear.$intMonth)
                    ->groupBy('tanggal')->paginate(12);

        foreach ($arrCM as $key => $value){
            $arrCM[$key]['cm1'] =  LogCall::where(DB::raw("date_format(created,'%d-%m-%Y')"),'=',$value->tanggal)->where('code','=','CALLME01')->count();
            $arrCM[$key]['cm2'] =  LogCall::where(DB::raw("date_format(created,'%d-%m-%Y')"),'=',$value->tanggal)->where('code','=','CALLME02')->count();
            $arrCM[$key]['cm3'] =  LogCall::where(DB::raw("date_format(created,'%d-%m-%Y')"),'=',$value->tanggal)->where('code','=','CALLME03')->count();
            $arrCM[$key]['com'] =  LogCall::where(DB::raw("date_format(created,'%d-%m-%Y')"),'=',$value->tanggal)->where('code','=','OUT016')->count();
        }

        return View::make('lokercm.report',compact('intMonth','intYear','arrCM','arrCs','intNew','intCM1','intCM2','intCM3','intQrt','intCom','arrData','totCom'))
            ->with('title','Report Call Me');
    }

    //Summary Report Call Me
    public function getSummary(){
        //date now configuration
        $intDate = (Input::has('date')) ? Input::get('date') : date('d-m-Y');

        $arrData = LokerCm::select('id','hand_phone','flag',DB::raw("date_format(created_at,'%d-%m-%Y') as tanggal"))
                ->where(DB::raw("date_format(created_at,'%d-%m-%Y')"),'=',$intDate)
                ->paginate(15);

        foreach ($arrData as $key => $value){
            $arrData[$key]['cm1'] = LogCall::where('cm_id','=',$value->id)->where('code','=','CALLME01')->first();
            $arrData[$key]['cm2'] = LogCall::where('cm_id','=',$value->id)->where('code','=','CALLME02')->first();
            $arrData[$key]['cm3'] = LogCall::where('cm_id','=',$value->id)->where('code','=','CALLME03')->first();
            $arrData[$key]['com'] = LogCall::where('cm_id','=',$value->id)->where('code','=','OUT016')->first();
        }

        return View::make('lokercm.summary',compact('arrData','intDate'))
            ->with('title','Report Call Me');
    }

    //Detail Daily Call Me Status
    public function getDaily($code = null){
        //date now configuration
        $intDate = (Input::has('date')) ? Input::get('date') : date('d-m-Y');
        $user_id = (Input::has('cs')) ? Input::get('cs') : '';

        $arrData = LogCall::with('CallMe')
                    ->with('userProfil')
                    ->where(DB::raw("date_format(created,'%d-%m-%Y')"),'=',$intDate)
                    ->where('code','=',$code)
                    ->where('cm_id','>',0)
                    ->where('user_id','LIKE','%'.$user_id.'%')
                    ->paginate(8);

        $arrCode = $code;
        /*
        echo '<pre>';
        echo print_r($arrData);
        die();
        */
        return View::make('lokercm.daily',compact('arrData','intDate','arrCode'))
            ->with('title','Report Call Me');
    }

    //Report CS Daily Call Me
    public function getCsdailyreport($user_id = null){
        //date configuration
        $intMonth = (Input::has('month')) ? Input::get('month') : date('m');
        $intYear  = (Input::has('year'))  ? Input::get('year')  : date('Y');
        $date_now = date('Ymd');

        $arrCs  = LogCall::with('UserProfil')
                ->select(DB::raw("count(*) as jumlah"),DB::raw("date_format(created,'%d-%m-%Y') as tanggal"),'user_id')
                ->where('user_id','=',$user_id)
                ->where(DB::raw("date_format(created,'%Y%m')"),'=',$intYear.$intMonth)
                ->groupBy('tanggal')
                ->paginate(14);

        foreach ($arrCs as $key => $value){
            $cm1    = LogCall::where('code','=','CALLME01')
                    ->where(DB::raw("date_format(created,'%d-%m-%Y')"),'=',$value->tanggal)
                    ->where('user_id','=',$user_id)
                    ->count();

            $cm2    = LogCall::where('code','=','CALLME02')
                    ->where(DB::raw("date_format(created,'%d-%m-%Y')"),'=',$value->tanggal)
                    ->where('user_id','=',$user_id)
                    ->count();

            $cm3    = LogCall::where('code','=','CALLME03')
                    ->where(DB::raw("date_format(created,'%d-%m-%Y')"),'=',$value->tanggal)
                    ->where('user_id','=',$user_id)
                    ->count();

            $com    = LogCall::where('code','=','OUT016')
                    ->where(DB::raw("date_format(created,'%d-%m-%Y')"),'=',$value->tanggal)
                    ->where('user_id','=',$user_id)
                    ->count();

            $arrCs[$key]['cm1'] = $cm1;
            $arrCs[$key]['cm2'] = $cm2;
            $arrCs[$key]['cm3'] = $cm3;
            $arrCs[$key]['com'] = $com;
        }

        return View::make('lokercm.csdaily',compact('arrCs','intMonth','intYear'))
            ->with('title','Report Call Me');
    }

    //Process Dummy Create Queue CM
    public function getQueue(){
        for ($i=0;$i<=8386;$i++){
            $callme = new LokerCm();
            $callme->code = Str::random(4);
            $callme->hand_phone = 6282213108387+$i;
            $callme->save();
        }
    }

    // Process Debugging
    public function getDebug(){
        //$arrCM = LokerCm::take(1)->get();
        //$arrCM = User::find(1);
        $files   = User::with('UserCertificates')->where('hand_phone','=','6282122437265')->first();
        $isUpload= $files->UserCertificates->count();

        //$upload= UserCertificate::where('user_id','=','158508')->get();
        //$upload= $user->UserFiles->count();
        //$files = UserCertificate::where('user_id','UserFiles')->count();

        //$id = $user->id;
        /*
        if ($arrCM) {
        /*
            foreach ($arrCM as $value){
                //$callme1    = LogCall::with('userProfil')->where('cm_id','=',$value->id)->where('code','=','OUT016')->get();
                //$callme2    = LogCall::with('userProfil')->where('cm_id','=',$value->id)->where('code','=','CALLME02')->get();
                //$files  = UserCertificate::where('user_id','=',$value->id)->get();
                $id = $value->id;
                //$arrCM[$key]['callme1'] = $callme1;
                //$arrCM[$key]['callme2'] = $callme2;
                //$arrCM['userUpload']= $files;

                /*
                foreach ($arrCM[$key]['callme1'] as $cm1 => $key) {
                    echo $key['user_id'];
                }
                */

                /*
                if (count($arrCM[$key]['callme1'])) {
                    echo '<pre>';
                    //echo print_r($arrCM);
                    //echo $arrCM;
                    echo $arrCM[$key]['callme1'][0]['user_profil']['firstname'];
                    //echo $arrCM[$key][0]['userProfil']['id'];
                }


            }
        */
            //$id = $arrCM->id;
            echo '<pre>';
            //echo print_r($files);
            var_dump($isUpload);
            die();


    }

    public function getBlast(){
        $arrUser   = DB::select( DB::raw("SELECT t1.hand_phone,t2.id as user_id,t2.sms_flag FROM loker_cms t1 LEFT JOIN users t2 ON t1.hand_phone=t2.hand_phone WHERE t1.flag = 4 and date_format(t1.created_at,'%Y%m%d') >= 20141210") );

        $tid       = '0';
        $smsContent= 'Kami sdh menghubungi km sebanyak 3 kali utk melengkapi pembuatan CV Anda, tp tdk ada respon. Mau di buatkan CV di SIM kamu? ketik Loker CM kirim ke 99910.';
        //$request = \Httpful\Request::get($uri)->send();
        if ($arrUser){
            foreach ($arrUser as $User){

                //Check User Subscription
                if ($User->sms_flag){
                    $strUrl     = 'http://202.43.162.235/responses/resetpasswordconfirmation?';
                    $strParam   = 'sms=' . urlencode($smsContent) . '&user_id=' . $User->user_id . '&tid=' . $tid;
                } else {
                    $strUrl     = 'http://202.43.162.235/responses/sendcodeverification?';
                    $strParam   = 'sms=' . urlencode($smsContent) . '&msisdn=' . $User->hand_phone;
                }
                //Send SMS Notification
                $mtRequest  = $strUrl.$strParam;
                \Httpful\Request::get($mtRequest)->send();

                echo $User->hand_phone . '   ' . 'OK' . "<br>";
            }
        }
    }

    /**
     * Debug Query
     */
    public function getCobaCoba(){
        $arrUser   = DB::select( DB::raw("select count(*) as jumlah,(select code from log_calls where code != 'OUT016' and log_calls.cm_id=loker_cms.id order by code desc limit 1) as code from loker_cms where flag = 1 and date_format(updated_at,'%Y%m') = 201502 group by code;") );
        $intCM1    = 0;
        $intCM2    = 0;
        $intCM3    = 0;
        foreach ($arrUser as $User){
            //count for cm1
            if ($User->code == 'CALLME01'){
                $intCM1 = $intCM1 + $User->jumlah;
            }
            //count for cm2
            if ($User->code == 'CALLME02'){
                $intCM2 = $intCM2 + $User->jumlah;
            }
            //count for cm3
            if ($User->code == 'CALLME03'){
                $intCM3 = $intCM3 + $User->jumlah;
            }
        }
        echo '<pre>';
        echo $intCM1 . '-' . $intCM2 . '-' . $intCM3;
    }
}