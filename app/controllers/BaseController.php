<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

    /**
     * Replace + and 0 with string 62, usually this is used for data phone.
     *
     * @param    str
     * @return   str
     */
    public function replacePlusAndZero($str){
        $regexFirstZero = "/^0/";
        $strAfterReplacePlus = str_replace("+", "", $str);
        $strAfterReplaceZero = preg_replace($regexFirstZero, '62', $strAfterReplacePlus);

        return $strAfterReplaceZero;
    }

}
