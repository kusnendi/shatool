<?php
/**
 * Created by PhpStorm.
 * User: Adicipta
 * Date: 10/24/2014
 * Time: 8:14 PM
 */

class ApplyJobController extends BaseController
{
    /** Requests
     * Handler*/
    public function getIndex(){
        $intDate1 = date('Y-m-d');
        $intDate2 = date('Y-m-d');
        $Applies = array();
        if (Input::has('start_date') && Input::has('end_date')){
            $Applies = ApplyJob::select(DB::raw("count(*) as jumlah"),DB::raw("date_format(created,'%Y-%m-%d') as tanggal"))
                    ->whereIn('user_id',function($query){
                        $query->select('id')
                            ->from('users')
                            ->where('role_id',4);
                    })
                    ->where(DB::raw("date_format(created,'%Y-%m-%d')"),'>=',Input::get('start_date'))
                    ->where(DB::raw("date_format(modified,'%Y-%m-%d')"),'<=',Input::get('end_date'))
                    ->groupBy('tanggal')
                    ->get();

            foreach ($Applies as $key => $value){
                $SentEmail = LogApplyJob::where(DB::raw("date_format(created_at,'%Y-%m-%d')"),$value->tanggal)
                            ->count();
                $Applies[$key]['intEmailSent'] = $SentEmail;
            }
            $intDate1 = Input::get('start_date');
            $intDate2 = Input::get('end_date');
        }

        return View::make('apply-job.index', compact('Applies','intDate1','intDate2'))
                ->with('title','Monitoring Sent Email Apply Jobs');
    }

    public function postLogEmailDetail(){
        $tanggal = Input::get('tanggal');
        $LogEmailDetail = LogApplyJob::where(DB::raw("date_format(created_at,'%Y-%m-%d')"),$tanggal)->get();

        return View::make('apply-job.mailsent', compact('LogEmailDetail'));
    }

    public function getApplicantDetail(){
        $intDate1 = date('Y-m-d');
        $Applies = '';
        if (Input::has('start_date')){
            $Applies = ApplyJob::whereIn('user_id',function($query){
                    $query->select('id')
                        ->from('users')
                        ->where('role_id',4);
                })
                ->where(DB::raw("date_format(created,'%Y-%m-%d')"),Input::get('start_date'))
                ->paginate(25);

            foreach ($Applies as $key => $value){
                $isEmail = LogApplyJob::where('applied_id', $value->id)->get();
                $Applies[$key]['Status'] = $isEmail;
            }

            $intDate1 = Input::get('start_date');
        }

        return View::make('apply-job.applicant-detail', compact('Applies','intDate1'))
            ->with('title','Detail Applicant Apply Jobs');
    }

    public function getResource(){
        $mtrUrl  = 'http://localhost:8000/requests?msisdn=085798535250&content=nendi&trx_id=23120012345';
        $request = \Httpful\Request::get($mtrUrl)->send();

        echo '<pre>';
        echo print_r($request);
    }

}