<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function getCs($uid = null)
	{
		//Save User id in session
        if ($uid != null){
            //store session user_id
            Session::put('csid',$uid);
        }

        $now   = date('Ymd');

        $arrUsr= UserProfile::where('user_id','=',$uid)->first();

        $arrCM = array(
            'req'   => LokerCm::where('flag','=','0')->count(),
            'cm1'   => LokerCm::where('flag','=','2')->count(),
            'cm2'   => LokerCm::where('flag','=','3')->count(),
            'cm3'   => LokerCm::where('flag','=','4')->count(),
        );

        $intCV = User::where('sms_flag','=','1')
            ->where('sms_subscription','regexp','reg')
            ->where('role_id','=','4')
            ->whereNotIn('id', function($query){
                $query->select('user_id')
                    ->from('user_profiles')
                    ->whereNotNull('firstname');
            })
            ->count();

        $arrOvr= array(
            'inc'   => LogCall::where('user_id','=',$uid)->where('type','=','incoming call')->whereRaw("DATE(created) = $now")->count(),
            'out'   => LogCall::where('user_id','=',$uid)->where('type','=','outgoing call')->whereRaw("DATE(created) = $now")->count(),
            'job'   => Job::where('user_id','=',$uid)->whereRaw("DATE(created) = $now")->count()
        );

        //Rendering View
        return View::make('dashboard.cs', compact('arrCM','arrOvr','arrUsr','intCV'))
            ->with('title', 'Dashboard | tloker.com');

	}

    public function getCsLite($uid = null)
    {
        //Save User id in session
        if ($uid != null){
            //store session user_id
            Session::put('csid',$uid);
        }

        $now   = date('Ymd');

        $arrUsr= UserProfile::where('user_id','=',$uid)->first();

        $arrCM = array(
            'req'   => LokerCm::where('flag','=','0')->count(),
            'cm1'   => LokerCm::where('flag','=','2')->count(),
            'cm2'   => LokerCm::where('flag','=','3')->count(),
            'cm3'   => LokerCm::where('flag','=','4')->count(),
        );

        $arrOvr= array(
            'inc'   => LogCall::where('user_id','=',$uid)->where('type','=','incoming call')->whereRaw("DATE(created) = $now")->count(),
            'out'   => LogCall::where('user_id','=',$uid)->where('type','=','outgoing call')->whereRaw("DATE(created) = $now")->count(),
            'job'   => Job::where('user_id','=',$uid)->whereRaw("DATE(created) = $now")->count()
        );

        //Rendering View
        return View::make('dashboard.cs-lite', compact('arrCM','arrOvr','arrUsr'))
            ->with('title', 'Dashboard | tloker.com');

    }

}