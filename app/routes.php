<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return '<center><h1>Service Not Available</h1></center>';
});

/* Home Controller */
Route::controller('dashboard', 'HomeController');

/* CallMe Controller */
Route::controller('callme', 'LokercmController');

/* CallMe Laters */
Route::controller('appointment', 'CallmeLaterController');

/* Users Controller */
Route::controller('user', 'UserController');

/* Jobs Controller */
Route::controller('job', 'JobController');

/* Companies Controller */
Route::controller('company', 'CompanyController');

Route::controller('requests', 'RequestsController');

/* Apply Job */
Route::controller('applyjob', 'ApplyJobController');