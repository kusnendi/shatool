<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusToCallmeLaters extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('callme_laters', function(Blueprint $table)
		{
			$table->tinyInteger("status")->default(0)->after('cm_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('callme_laters', function(Blueprint $table)
		{
			$table->dropColumn('status');
		});
	}

}
