<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('loker_cms', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('hand_phone',50);
            $table->string('code',4);
            $table->tinyInteger('flag');
            $table->text('notes');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('loker_cms');
	}

}
