<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCallmeLaters extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('callme_laters', function(Blueprint $table)
		{
			$table->increments("id");
            $table->string("hand_phone",100);
            $table->dateTime("callme_back");
            $table->text("notes");
            $table->integer("cm_id");
            $table->integer("user_id");
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('callme_laters');
	}

}
