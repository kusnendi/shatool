<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsBlastToLokerCms extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('loker_cms', function(Blueprint $table)
		{
			$table->tinyInteger("is_blast")->default(1)->after('flag');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('loker_cms', function(Blueprint $table)
		{
			$table->dropColumn('is_blast');
		});
	}

}
